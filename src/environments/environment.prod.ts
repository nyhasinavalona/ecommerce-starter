export const environment = {
    production: true,
    apiBaseUrl: ``,
    mock: {
        home: true,
        auth: true,
        payment: true,
        order: true
    }
};
