/* tslint:disable */
import { Injectable } from '@angular/core';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { HomeTab } from 'src/app/shared/interfaces/home-tab.interface';
import { User } from 'src/app/shared/models/user.model';

export interface NotificationsCard {
    image: string;
    title: string;
    time: number;
}

export interface Notification {
    all: Array<NotificationsCard>;
    deals: Array<NotificationsCard>;
    orders: Array<NotificationsCard>;
    others: Array<NotificationsCard>;
}

export interface Product {
    name: string;
    image: Array<string>;
    size: string;
    color: string;
    price: number;
    discount: number;
    offer: boolean;
    stock: number;
    description: string;
    currency: string;
    bought: number;
    shipping: number;
    rating: number;
    ratingCount: number;
    storeRate: number;
    storeRating: number;
    storeRatingCount: number;
    soldBy: string;
    specs: string;
    reviews: Array<Review>;
    storeReviews: Array<Review>;
    sizing: {
        small: number;
        okay: number;
        large: number;
    };
    buyerGuarantee: string;
    sponsored: Array<Product>;
}
export interface Review {
    image: string;
    name: string;
    comment: string;
    rating: number;
    images: Array<string>;
}

export interface Orders {
    product: Product;
    order_date: Date;
    id: string;
    amount: number;
    delivery_date: Date;
    status: string;
    billing_address: Address;
    shipping_address: Address;
    tax: number;
}

@Injectable({
    providedIn: 'root'
})
export class DataService {
    constructor() {}

    terms_of_use =
        'The Terms and Conditions agreement can act as a legal contract between you, the mobile app owner or developer, and the users of your app. Like a Terms and Conditions for a website, this agreement for a mobile app would set the rules and terms that users must follow in order to use your app.' +
        "Here are a couple of reasons why you'll want to have a Terms and Conditions for a mobile app:" +
        'You can stop abusive users from using your app.' +
        'You can terminate or block accounts at your sole discretion.' +
        'Liability to users will be limited.' +
        'And many more.' +
        "If you don't have this agreement for your mobile app yet, use the Generator to create it!";
    privacy_policy =
        "You'll need the Privacy Policy agreement even if you don't collect any personal data yourself through the mobile app you're building, but instead use third party tools such as:" +
        '- Google Analytics Mobile' +
        '- Flurry' +
        '- Firebase' +
        '- Mixpanel' +
        'And so on' +
        'If you use at least one third party tool that might collect personal data through your mobile app, you need this agreement in place.' +
        'Each app store also requires you to have this agreement in place before submitting the mobile app:' +
        '- Apple App Store' +
        '- Google Play Store' +
        '- Microsoft Windows Phone Store';

    card: NotificationsCard = {
        image: 'assets/images/products/1.jpg',
        title: 'Kya aapne kabhi online hotel book kia hai???\nHotel? Sastago',
        time: 9
    };

    notifications: Notification = {
        all: [
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card
        ],
        deals: [
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card
        ],
        orders: [],
        others: [
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card,
            this.card
        ]
    };

    trending = [
        'jacket',
        'drone',
        'shoes for men',
        'car accessories',
        'blazer for men',
        'watches men',
        'sports shoes for men',
        'earphone bluetooth',
        'jackets for men',
        'memory card'
    ];

    notifications_tab: Array<HomeTab> = [
        { title: 'All' },
        { title: 'Deals' },
        { title: 'Your Orders' },
        { title: 'Other' }
    ];

    rewards_tab: Array<HomeTab> = [
        { title: 'Dashboard' },
        { title: 'Redeem' },
        { title: 'Information' }
    ];

    rewards = {
        points: 100,
        since: new Date(),
        available: [
            { discount: 5, code: 'ABCDEF', expire: new Date(), expired: false }
        ],
        used: [
            { discount: 10, code: 'XEFGSD', expire: new Date(), expired: true }
        ],
        redeem: [
            { discount: 5, points: 200 },
            { discount: 10, points: 600 },
            { discount: 15, points: 1000 }
        ]
    };

    cart: Array<CartItem> = [];

    current_user: User = new User().deserialize({
        firstName: 'Tanay',
        uid: 'ALSIOCSIIUAISUC',
        did: 'JIOU-ASBB-C871-0345',
        aid: 'ASBB-ASBB-C871-0345',
        lastName: 'Toshniwal',
        email: 'tanaytoshniwal98@gmail.com',
        billing: [
            { card_number: '3124', expiry_date: '12/22' },
            { card_number: '4564', expiry_date: '03/25' }
        ],
        address: [
            {
                firstAddressLine: 'ghar',
                secondAddressLine: 'ghar',
                city: 'jaipur',
                lastName: 'bond',
                phoneNumber: 1125532553,
                zipCode: 12345,
                country: 'India',
                firstName: 'James',
                state: 'Rajasthan'
            },
            {
                firstAddressLine: 'office',
                secondAddressLine: 'Office',
                city: 'Delhi',
                lastName: 'Holmes',
                phoneNumber: 1125532553,
                zipCode: 12345,
                country: 'India',
                firstName: 'Sherlock',
                state: 'Delhi'
            }
        ]
    });

    wish_cash = {
        currency: '$',
        amount: 0.0,
        history: [{ amount: 10 }, { amount: 20 }]
    };

    orders: Array<Orders> = [];

    faqs = {
        'Shipping and Delivery': [
            {
                'How log does shipping take?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How can I track my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How much does shipping cost?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Where does my order ship from?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I change my shipping address?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        'Returns and Refunds': [
            {
                'How do I return a product?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can I exchange an item?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I cancel my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        'Payment, Pricing & Promotions': [
            {
                'How do I return a product?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can I exchange an item?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I cancel my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        Orders: [
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        'Managing Your Account': [
            {
                'How do I return a product?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can I exchange an item?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I cancel my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        'User Feedback': [
            {
                'How do I return a product?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can I exchange an item?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I cancel my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ],
        'Customer Support': [
            {
                'How do I return a product?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can I exchange an item?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'How do I cancel my order?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                "What's the status of my refund?":
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            },
            {
                'I cancelled my order. How will I receive my refund?':
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.'
            }
        ]
    };
}
