import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppEffects } from 'src/app/app.effects';
import { AppGuard } from 'src/app/app.guard';
import { metaReducers, reducers } from 'src/app/app.reducers';
import { AddressBookEffects } from 'src/app/pages/address-book/store/address-book.effects';
import { AuthEffects } from 'src/app/pages/auth/auth.effects';
import { AuthMockService } from 'src/app/pages/auth/services/auth-mock.service';
import { AuthService } from 'src/app/pages/auth/services/auth.service';
import { OrderMockService } from 'src/app/pages/cart/services/order-mock.service';
import { OrderService } from 'src/app/pages/cart/services/order.service';
import { CartEffects } from 'src/app/pages/cart/store/cart.effects';
import { InfoModalPage } from 'src/app/pages/info-modal/info-modal.page';
import { PaymentsEffects } from 'src/app/pages/manage-payments/store/payments.effects';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [AppComponent, InfoModalPage],
    entryComponents: [InfoModalPage],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        StoreModule.forRoot(reducers, { metaReducers }),
        StoreRouterConnectingModule.forRoot(),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production
        }),
        EffectsModule.forRoot([
            AppEffects,
            AuthEffects,
            CartEffects,
            PaymentsEffects,
            AddressBookEffects
        ])
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AppGuard,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        {
            provide: AuthService,
            useClass: environment.mock.auth ? AuthMockService : AuthService
        },
        {
            provide: OrderService,
            useClass: environment.mock.order ? OrderMockService : OrderService
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
