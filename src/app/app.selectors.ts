import { getSelectors } from '@ngrx/router-store'
import { createSelector } from '@ngrx/store'
import { AppState } from 'src/app/app.reducers'

export const selectRouterState = (state: AppState) => state.router;
export const selectAuthState = (state: AppState) => state.auth;
export const selectHomeState = (state: AppState) => state.home;
export const selectProductDetailsState = (state: AppState) =>
    state.productDetails;
export const selectCartState = (state: AppState) => state.cart;
export const selectAddressState = (state: AppState) => state.address;
export const selectAddressBookState = (state: AppState) => state.addressBook;
export const selectPaymentsState = (state: AppState) => state.payments;
export const selectPaymentState = (state: AppState) => state.payment;

export const {
    selectCurrentRoute,
    selectQueryParam,
    selectQueryParams,
    selectRouteData,
    selectRouteParam,
    selectRouteParams,
    selectUrl
} = getSelectors(selectRouterState);

export const selectIsCreationMode = createSelector(selectUrl, url =>
    url.includes('new')
);
