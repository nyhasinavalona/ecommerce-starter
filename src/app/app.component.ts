import { Component, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MAIN_PAGES, SECONDARY_PAGES } from 'src/app/app.constants';
import { AppState } from 'src/app/app.reducers';
import { selectUser } from 'src/app/pages/auth/auth.selectors';
import { User } from 'src/app/shared/models/user.model';
import { UtilsService } from 'src/app/utils.service';
import { DataService } from './data.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
    side_open = true;
    side_open1 = true;
    mainPages = MAIN_PAGES;
    secondaryPages = SECONDARY_PAGES;
    user$: Observable<User>;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public dataService: DataService,
        public utilsService: UtilsService,
        private store: Store<AppState>
    ) {
        this.initializeApp();
    }

    ngOnInit(): void {
        this.user$ = this.store.pipe(select(selectUser));
    }

    menu(b) {
        if (b) {
            this.side_open = false;
            this.side_open1 = true;
        } else {
            this.side_open = false;
            this.side_open1 = false;
        }
    }

    back() {
        this.side_open = true;
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            // this.splashScreen.hide();
        });
    }
}
