import { Page } from 'src/app/shared/interfaces/page.interface';

export const MAIN_PAGES: Page[] = [
    { title: 'Acheter', url: '/home', icon: 'home' },
    { title: 'Rechercher', url: '/search', modal: true, icon: 'search' },
    { title: 'Notifications', url: '/notification', icon: 'notifications' },
    { title: 'Panier', url: '/cart', icon: 'cart' },
    { title: 'Historique des commandes', url: '/orders', icon: 'list' }
];
export const SECONDARY_PAGES = [
    { title: 'Service client', url: '/support', icon: 'people' },
    { title: 'FAQs', url: '/faqs', icon: 'help-circle' },
    { title: 'Paramètres', url: '/settings', icon: 'cog' }
];
