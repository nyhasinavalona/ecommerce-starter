import { routerReducer, RouterReducerState } from '@ngrx/router-store'
import { ActionReducer, ActionReducerMap } from '@ngrx/store'
import { localStorageSync } from 'ngrx-store-localstorage'
import { addressBookReducer, AddressBookState } from 'src/app/pages/address-book/store/address-book.reducers'
import { authReducer, AuthState } from 'src/app/pages/auth/auth.reducers'
import { cartReducer, CartState } from 'src/app/pages/cart/store/cart.reducers'
import { homeReducer, HomeState } from 'src/app/pages/home/store/home.reducers'
import { paymentsReducer, PaymentsState } from 'src/app/pages/manage-payments/store/payments.reducers'
import { addressReducer, AddressState } from 'src/app/pages/new-address/store/new-address.reducers'
import { paymentReducer, PaymentState } from 'src/app/pages/new-payment/store/new-payment.reducers'
import {
    productDetailsReducer,
    ProductDetailsState,
} from 'src/app/pages/product-details/store/product-details.reducers'

export interface AppState {
    router: RouterReducerState<any>;
    auth: AuthState;
    home: HomeState;
    productDetails: ProductDetailsState;
    cart: CartState;
    address: AddressState;
    addressBook: AddressBookState;
    payments: PaymentsState;
    payment: PaymentState;
}

export const reducers: ActionReducerMap<AppState> = {
    router: routerReducer,
    auth: authReducer,
    home: homeReducer,
    productDetails: productDetailsReducer,
    cart: cartReducer,
    address: addressReducer,
    addressBook: addressBookReducer,
    payments: paymentsReducer,
    payment: paymentReducer
};

export function localStorageSyncReducer(
    reducer: ActionReducer<any>
): ActionReducer<any> {
    return localStorageSync({ keys: ['auth'], rehydrate: true })(reducer);
}

export const metaReducers = [localStorageSyncReducer];
