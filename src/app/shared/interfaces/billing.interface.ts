export interface Billing {
    id: number;
    phone: string;
    firstName: string;
    lastName: string;
}
