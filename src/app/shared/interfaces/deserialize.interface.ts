export interface IDeserialize {
    deserialize(input: any): this;
}
