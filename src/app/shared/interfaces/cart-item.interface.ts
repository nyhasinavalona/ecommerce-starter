import { Product } from 'src/app/shared/models/product.model';

export interface CartItem {
    product: Product;
    quantity: number;
}
