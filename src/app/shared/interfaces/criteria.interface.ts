export interface Criteria<T> {
    search?: string;
    sort?: Sort<T>;
    pagination?: Pagination;
}

export enum SortDirection {
    ASC = 'asc',
    DESC = 'desc'
}

export interface Sort<T> {
    by: keyof T;
    direction: SortDirection;
}

export interface Pagination {
    skip: number;
    limit: number;
}

export interface Paginated<T> {
    items: T[];
    length: number;
}
