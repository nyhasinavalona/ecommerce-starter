export interface Address {
    id: number;
    firstName: string;
    lastName: string;
    firstAddressLine: string;
    secondAddressLine: string;
    region: string;
    city: string;
    zipCode: number;
    phone: number;
}
