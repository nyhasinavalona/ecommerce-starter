export const flatten = <T>(object: any) =>
    Object.assign(
        {},
        ...Object.entries(object)
            .filter(([key, value]) => value)
            .map(([key, value]) =>
                isObject(value) ? flatten(value) : { [key]: value }
            )
    );

export function isObject(o) {
    return o instanceof Object && o.constructor === Object;
}
