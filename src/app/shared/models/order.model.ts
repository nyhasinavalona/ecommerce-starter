import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { BaseModel } from 'src/app/shared/models/base.model';

export class Order extends BaseModel {
    meanOfPayment: any;
    deliveryAddress: any;
    items: CartItem[];
    cost: number;
    shipppingCost: number;
    user?: number;

    constructor() {
        super();
    }
}
