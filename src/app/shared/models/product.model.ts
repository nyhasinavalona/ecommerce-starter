import { Review } from 'src/app/data.service';
import { ProductCategory } from 'src/app/pages/home/home.constants';
import { BaseModel } from 'src/app/shared/models/base.model';

export interface Sizing {
    small: number;
    okay: number;
    large: number;
}

export class Product extends BaseModel {
    name: string;
    category: ProductCategory;
    image: string[];
    size: string;
    color: string;
    price: number;
    discount: number;
    offer: boolean;
    stock: number;
    description: string;
    currency: string;
    bought: number;
    shipping: number;
    rating: number;
    ratingCount: number;
    storeRate: number;
    storeRating: number;
    storeRatingOount: number;
    soldBy: string;
    specs: string;
    reviews: Review[];
    storeReviews: Review[];
    sizing: Sizing;
    buyerGuarantee: string;
    sponsored: Product[];

    constructor() {
        super();
    }
}
