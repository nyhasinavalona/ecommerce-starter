import { Address } from 'src/app/shared/interfaces/address.interface';
import { Billing } from 'src/app/shared/interfaces/billing.interface';
import { BaseModel } from 'src/app/shared/models/base.model';

export class User extends BaseModel {
    firstName: string;
    lastName: string;
    email: string;
    password?: string;
    address: Address[];
    billing: Billing[];
    profile?: string;

    constructor() {
        super();
    }
}
