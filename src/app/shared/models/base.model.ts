import { IDeserialize } from '../interfaces/deserialize.interface';
import { ISerialize } from '../interfaces/serialize.interface';

export class BaseModel implements ISerialize, IDeserialize {
    id: number;

    constructor() {}

    serialize(): object {
        return JSON.parse(JSON.stringify(this));
    }

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
