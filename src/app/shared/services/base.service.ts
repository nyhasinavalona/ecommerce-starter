import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { flatten } from '../helpers/flatten.helper';
import { ApiResponse } from '../interfaces/api-response.interface';
import { Criteria } from '../interfaces/criteria.interface';
import { BaseModel } from '../models/base.model';

export class BaseService<T extends BaseModel> {
    constructor(
        protected http: HttpClient,
        private Constructor: new () => T,
        private url: string
    ) {}

    loadAll(criteria: Criteria<T> = {}): Observable<T[]> {
        return this.http
            .get(`${environment.apiBaseUrl}/${this.url}`, {
                params: flatten(criteria)
            })
            .pipe(
                map((response: ApiResponse) => response.data),
                map((data: T[]) =>
                    data.map(item => new this.Constructor().deserialize(item))
                )
            );
    }

    loadById(id: number): Observable<T> {
        return this.http
            .get(`${environment.apiBaseUrl}/${this.url}/${id}`)
            .pipe(
                map((response: ApiResponse) => response.data),
                map((response: T) =>
                    new this.Constructor().deserialize(response)
                )
            );
    }

    new(payload: T): Observable<T> {
        return of(payload);
    }

    save(payload: T): Observable<T> {
        return !!payload.id ? this.update(payload) : this.add(payload);
    }

    add(payload: T): Observable<T> {
        return this.http
            .post(`${environment.apiBaseUrl}/${this.url}`, payload.serialize())
            .pipe(
                map((response: ApiResponse) => response.data),
                map((response: T) =>
                    new this.Constructor().deserialize(response)
                )
            );
    }

    update(payload: T): Observable<T> {
        return this.http
            .put(
                `${environment.apiBaseUrl}/${this.url}/${payload.id}`,
                payload.serialize()
            )
            .pipe(
                map((response: ApiResponse) => response.data),
                map((response: T) =>
                    new this.Constructor().deserialize(response)
                )
            );
    }

    delete(id: string): Observable<boolean> {
        return this.http
            .delete(`${environment.apiBaseUrl}/${this.url}/${id}`)
            .pipe(map(response => true));
    }
}
