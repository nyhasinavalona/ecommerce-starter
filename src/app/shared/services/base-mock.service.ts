import { Observable, of } from 'rxjs';
import {
    Criteria,
    Pagination,
    Sort,
    SortDirection
} from '../interfaces/criteria.interface';
import { BaseModel } from '../models/base.model';

export class BaseMockService<T extends BaseModel> {
    constructor(private Constructor: new () => T, protected mocks: T[]) {}

    load(criteria?: Criteria<T>): Observable<T[]> {
        if (criteria.sort && criteria.sort.by && criteria.sort.direction) {
            this.sort(criteria.sort);
        }
        const paginated = this.paginate(criteria.pagination);
        return of(paginated);
    }

    loadAll(criteria?: Criteria<T>): Observable<T[]> {
        return of(this.mocks);
    }

    loadById(id: string): Observable<T> {
        return of(this.mocks.find(item => +item.id === +id));
    }

    new(value: T): Observable<T> {
        return of(new this.Constructor().deserialize(value));
    }

    save(payload: T): Observable<T> {
        return !!payload.id ? this.update(payload) : this.add(payload);
    }

    add(payload: T): Observable<T> {
        const savedPayload = new this.Constructor().deserialize({
            ...payload.serialize(),
            id: this.mocks.length + 1
        });
        this.mocks.push(savedPayload);
        return of(savedPayload);
    }

    update(payload: T): Observable<T> {
        this.mocks = this.mocks.map(item =>
            item.id === payload.id ? payload : item
        );
        return of(payload);
    }

    delete(id: number): Observable<boolean> {
        this.mocks = this.mocks.filter(item => item.id !== id);
        return of(true);
    }

    private paginate(pagination: Pagination): T[] {
        const start = pagination.skip * (pagination.skip - 1);
        const end = start + pagination.limit;
        return this.mocks.slice(start, end);
    }

    private sort(sort: Sort<T>) {
        const direction = SortDirection.ASC ? 1 : -1;
        this.mocks.sort((a, b) =>
            a[sort.by] < b[sort.by] ? direction : -direction
        );
    }
}
