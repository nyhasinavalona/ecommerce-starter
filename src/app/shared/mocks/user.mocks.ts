import { User } from 'src/app/shared/models/user.model';

export const USER_MOCKS: User[] = [
    {
        id: 1,
        firstName: 'Ny Hasinavalona',
        lastName: 'Randriantsarafara',
        email: 'randriantsarafaranyhasinavalon@gmail.com',
        address: [
            {
                id: 1,
                firstAddressLine: 'II N 191 DA',
                secondAddressLine: 'Anjanahary',
                phone: 1125532553,
                city: 'ANTANANARIVO',
                zipCode: 12345,
                region: 'ANALAMANGA',
                firstName: 'Ny Hasinavalona',
                lastName: 'Randriantsarafara'
            }
        ],
        billing: [
            {
                phone: '261340337608',
                firstName: 'Ny Hasinavalona',
                lastName: 'Randriantsarafara'
            }
        ]
    }
].map(item => new User().deserialize(item));
