import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigationAction } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AppState } from 'src/app/app.reducers';
import { selectRouteParam } from 'src/app/app.selectors';
import { HomeService } from 'src/app/pages/home/services/home.service';
import {
    loadProduct,
    loadProductFail,
    loadProductSuccess,
    loadRelatedProducts,
    loadRelatedProductsFail,
    loadRelatedProductsSuccess
} from 'src/app/pages/product-details/store/product-details.actions';

@Injectable()
export class ProductDetailsEffects {
    productResolver$ = createEffect(() =>
        this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(
                this.store.pipe(select(selectRouteParam('productId')))
            ),
            map(([_, id]) => loadProduct({ id: +id }))
        )
    );

    loadProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadProduct),
            switchMap(({ id }) =>
                this.homeService.loadById(id).pipe(
                    map(product => loadProductSuccess({ product })),
                    catchError(error => of(loadProductFail({ error })))
                )
            )
        )
    );

    loadProductSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadProductSuccess),
            map(({ product }) =>
                loadRelatedProducts({
                    criteria: { category: product ? product.category : null }
                })
            )
        )
    );

    loadRelatedProducts$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadRelatedProducts),
            switchMap(({ criteria }) =>
                this.homeService.loadAll(criteria).pipe(
                    map(products => loadRelatedProductsSuccess({ products })),
                    catchError(error => of(loadRelatedProductsFail({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private homeService: HomeService,
        private store: Store<AppState>
    ) {}
}
