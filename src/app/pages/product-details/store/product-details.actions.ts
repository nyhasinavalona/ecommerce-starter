import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ProductCriteria } from 'src/app/pages/home/interfaces/product-criteria.interface';
import { Product } from 'src/app/shared/models/product.model';

export const loadProduct = createAction(
    'LOAD_PRODUCT',
    props<{ id: number }>()
);
export const loadProductFail = createAction(
    'LOAD_PRODUCT_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const loadProductSuccess = createAction(
    'LOAD_PRODUCT_SUCCESS',
    props<{ product: Product }>()
);
export const loadRelatedProducts = createAction(
    'LOAD_RELATED_PRODUCTS',
    props<{ criteria: ProductCriteria }>()
);
export const loadRelatedProductsFail = createAction(
    'LOAD_RELATED_PRODUCTS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const loadRelatedProductsSuccess = createAction(
    'LOAD_RELATED_PRODUCTS_SUCCESS',
    props<{ products: Product[] }>()
);
