import { createSelector } from '@ngrx/store';
import { selectProductDetailsState } from 'src/app/app.selectors';

export const selectProduct = createSelector(
    selectProductDetailsState,
    state => state.product
);

export const selectRelatedProducts = createSelector(
    selectProductDetailsState,
    state => state.relatedProducts
);
