import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import {
    loadProduct,
    loadProductFail,
    loadProductSuccess,
    loadRelatedProducts,
    loadRelatedProductsFail,
    loadRelatedProductsSuccess
} from 'src/app/pages/product-details/store/product-details.actions';
import { Product } from 'src/app/shared/models/product.model';

export interface ProductDetailsState {
    product: Product;
    relatedProducts: Product[];
    error: HttpErrorResponse;
}

export const initialState: ProductDetailsState = {
    product: null,
    relatedProducts: [],
    error: null
};

const reducer = createReducer(
    initialState,
    on(loadProduct, state => ({ ...state, product: null })),
    on(loadProductFail, (state, { error }) => ({ ...state, error })),
    on(loadProductSuccess, (state, { product }) => ({ ...state, product })),
    on(loadRelatedProducts, state => ({ ...state, relatedProducts: [] })),
    on(loadRelatedProductsFail, (state, { error }) => ({ ...state, error })),
    on(loadRelatedProductsSuccess, (state, { products }) => ({
        ...state,
        relatedProducts: products
    }))
);

export function productDetailsReducer(state, action) {
    return reducer(state, action);
}
