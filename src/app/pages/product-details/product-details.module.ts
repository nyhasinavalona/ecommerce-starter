import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { HomeMockService } from 'src/app/pages/home/services/home-mock.service';
import { HomeService } from 'src/app/pages/home/services/home.service';
import { InnerHomeComponent } from 'src/app/pages/product-details/components/inner-home/inner-home.component';
import { ProductComponent } from 'src/app/pages/product-details/components/product/product.component';
import { ProductDetailsPage } from 'src/app/pages/product-details/product-details.page';
import { ProductDetailsEffects } from 'src/app/pages/product-details/store/product-details.effects';
import { ReviewComponent } from 'src/app/pages/review/review.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    {
        path: ':productId',
        component: ProductDetailsPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        EffectsModule.forFeature([ProductDetailsEffects])
    ],
    providers: [
        SocialSharing,
        {
            provide: HomeService,
            useClass: environment.mock.home ? HomeMockService : HomeService
        }
    ],
    declarations: [
        ProductDetailsPage,
        ProductComponent,
        InnerHomeComponent,
        ReviewComponent
    ],
    entryComponents: [ProductComponent, InnerHomeComponent, ReviewComponent]
})
export class ProductDetailsPageModule {}
