import { HomeTab } from 'src/app/shared/interfaces/home-tab.interface';

export enum ProductDetailsTab {
    OVERVIEW = 'OVERVIEW',
    RELATED = 'RELATED',
    REVIEW = 'REVIEW',
    STORE_REVIEW = 'STORE_REVIEW'
}

export const PRODUCT_DETAILS_TAB_LABELS = {
    [ProductDetailsTab.OVERVIEW]: 'Aperçu',
    [ProductDetailsTab.RELATED]: 'Produits connexe',
    [ProductDetailsTab.REVIEW]: 'Avis sur le produit',
    [ProductDetailsTab.STORE_REVIEW]: 'Avis sur le magasin'
};

export const PRODUCT_DETAILS_TABS: HomeTab[] = Object.values(
    ProductDetailsTab
).map(value => ({ title: value }));
