import { Component, OnInit, ViewChild } from '@angular/core';
import {
    IonContent,
    IonSlides,
    MenuController,
    NavController
} from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import { addItem } from 'src/app/pages/cart/store/cart.actions';
import {
    PRODUCT_DETAILS_TAB_LABELS,
    PRODUCT_DETAILS_TABS,
    ProductDetailsTab
} from 'src/app/pages/product-details/product-details.constants';
import {
    selectProduct,
    selectRelatedProducts
} from 'src/app/pages/product-details/store/product-details.selectors';
import { HomeTab } from 'src/app/shared/interfaces/home-tab.interface';
import { Product } from 'src/app/shared/models/product.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.page.html',
    styleUrls: ['./product-details.page.scss']
})
export class ProductDetailsPage implements OnInit {
    @ViewChild('slides') slides: IonSlides;
    @ViewChild('content') content: IonContent;
    @ViewChild('slider') slider: IonSlides;
    index = 0;
    segment = '';
    slideOpts = {
        effect: 'flip',
        zoom: false
    };
    tabs: HomeTab[] = [];
    tabLabels = PRODUCT_DETAILS_TAB_LABELS;
    product$: Observable<Product>;
    relatedProducts$: Observable<Product[]>;

    constructor(
        private menuCtrl: MenuController,
        private fun: UtilsService,
        private dataService: DataService,
        private nav: NavController,
        private store: Store<AppState>
    ) {}

    get ProductDetailsTab() {
        return ProductDetailsTab;
    }

    ngOnInit() {
        this.product$ = this.store.pipe(select(selectProduct));
        this.relatedProducts$ = this.store.pipe(select(selectRelatedProducts));
        this.tabs = PRODUCT_DETAILS_TABS;
        this.segment = this.tabs[0].title;
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    async change() {
        await this.slides.getActiveIndex().then(d => (this.index = d));
        this.segment = this.tabs[this.index].title;
        this.drag();
    }

    onReviewClick(index) {
        this.segment = this.tabs[index].title;
        this.index = index;
        this.slides.slideTo(index);
        this.content.scrollToTop();
        this.drag();
    }

    addToCart() {
        this.store.dispatch(addItem());
    }

    onSelect(i) {
        this.slides.slideTo(i);
    }

    drag() {
        let distanceToScroll = 0;
        for (const index in this.tabs) {
            if (+index < this.index) {
                distanceToScroll =
                    distanceToScroll +
                    document.getElementById('seg_' + index).offsetWidth +
                    24;
            }
        }
        document.getElementById('dag').scrollLeft = distanceToScroll;
    }

    onSegmentChange(event) {
        this.segment = event.detail.value;
    }
}
