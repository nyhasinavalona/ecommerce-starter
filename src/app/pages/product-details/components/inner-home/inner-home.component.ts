import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Product } from 'src/app/shared/models/product.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-inner-home',
    templateUrl: './inner-home.component.html',
    styleUrls: ['./inner-home.component.scss']
})
export class InnerHomeComponent implements OnInit {
    @Input() items: Product[];

    constructor(
        private fun: UtilsService,
        private navController: NavController
    ) {}

    ngOnInit() {}

    open(product: Product) {
        this.navController.navigateForward(`/product-details/${product.id}`);
    }
}
