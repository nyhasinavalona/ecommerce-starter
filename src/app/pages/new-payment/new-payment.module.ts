import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { NewPaymentPage } from 'src/app/pages/new-payment/new-payment.page';
import { PaymentFormComponent } from 'src/app/pages/new-payment/components/payment-form/payment-form.component';
import { BillingService } from 'src/app/pages/new-payment/services/billing.service';
import { NewPaymentEffects } from 'src/app/pages/new-payment/store/new-payment.effects';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    {
        path: 'new',
        component: NewPaymentPage
    },
    {
        path: 'edit/:paymentId',
        component: NewPaymentPage
    },
    {
        path: '',
        redirectTo: 'new',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        EffectsModule.forFeature([NewPaymentEffects])
    ],
    declarations: [NewPaymentPage, PaymentFormComponent],
    providers: [BillingService]
})
export class NewpaymentPageModule {}
