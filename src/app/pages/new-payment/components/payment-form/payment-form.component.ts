import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Billing } from 'src/app/shared/interfaces/billing.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-payment-form',
    templateUrl: './payment-form.component.html',
    styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit, OnChanges {
    @Input() billing: Billing;
    @Output() save: EventEmitter<Billing> = new EventEmitter<Billing>();
    form: FormGroup;
    constructor(
        private formBuilder: FormBuilder,
        private utilsService: UtilsService
    ) {}

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.billing) {
            this.form = this.initForm(this.billing);
        }
    }

    onSubmit() {
        if (this.form.valid) {
            this.save.emit(this.form.value);
        } else {
            this.utilsService.presentToast(
                'Des champs sont invalides.',
                true,
                'top',
                1500
            );
        }
    }

    private initForm(billing: Billing): FormGroup {
        return this.formBuilder.group({
            id: [billing.id],
            phone: [billing.phone],
            lastName: [billing.lastName],
            firstName: [billing.firstName]
        });
    }
}
