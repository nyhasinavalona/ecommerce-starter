import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { saveBilling } from 'src/app/pages/new-payment/store/new-payment.actions';
import { selectBilling } from 'src/app/pages/new-payment/store/new-payment.selectors';
import { Billing } from 'src/app/shared/interfaces/billing.interface';

@Component({
    selector: 'app-new-payment',
    templateUrl: './new-payment.page.html',
    styleUrls: ['./new-payment.page.scss']
})
export class NewPaymentPage implements OnInit {
    billing$: Observable<Billing>;

    constructor(
        private menuCtrl: MenuController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.billing$ = this.store.pipe(select(selectBilling));
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    onSave(billing: Billing) {
        this.store.dispatch(saveBilling({ billing }));
    }
}
