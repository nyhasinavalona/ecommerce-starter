import { createReducer, on } from '@ngrx/store';
import {
    createBillingSuccess,
    loadBillingSuccess
} from 'src/app/pages/new-payment/store/new-payment.actions';
import { Billing } from 'src/app/shared/interfaces/billing.interface';

export interface PaymentState {
    billing: Billing;
}

export const initialState: PaymentState = {
    billing: null
};

const reducer = createReducer(
    initialState,
    on(createBillingSuccess, (state, { billing }) => ({ ...state, billing })),
    on(loadBillingSuccess, (state, { billing }) => ({ ...state, billing }))
);

export function paymentReducer(state, action) {
    return reducer(state, action);
}
