import { HttpErrorResponse } from '@angular/common/http'
import { createAction, props } from '@ngrx/store'
import { Billing } from 'src/app/shared/interfaces/billing.interface'
import { Error } from 'tslint/lib/error'

export const loadBilling = createAction(
    'LOAD_BILLING',
    props<{ id: number }>()
);
export const loadBillingFail = createAction(
    'LOAD_BILLING_FAIL',
    props<{ error: Error }>()
);
export const loadBillingSuccess = createAction(
    'LOAD_BILLING_SUCCESS',
    props<{ billing: Billing }>()
);
export const createBilling = createAction('CREATE_BILLING');
export const createBillingFail = createAction(
    'CREATE_BILLING_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const createBillingSuccess = createAction(
    'CREATE_BILLING_SUCCESS',
    props<{ billing: Billing }>()
);
export const saveBilling = createAction(
    'SAVE_BILLING',
    props<{ billing: Billing }>()
);
export const saveBillingFail = createAction(
    'SAVE_BILLING_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const saveBillingSuccess = createAction(
    'SAVE_BILLING_SUCCESS',
    props<{ billing: Billing }>()
);
