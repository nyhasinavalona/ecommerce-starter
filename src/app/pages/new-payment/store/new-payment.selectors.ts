import { createSelector } from '@ngrx/store'
import { selectPaymentState } from 'src/app/app.selectors'

export const selectBilling = createSelector(
    selectPaymentState,
    state => state.billing
);
