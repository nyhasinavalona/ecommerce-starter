import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigationAction } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
    catchError,
    filter,
    map,
    switchMap,
    tap,
    withLatestFrom
} from 'rxjs/operators';
import { AppState } from 'src/app/app.reducers';
import {
    selectIsCreationMode,
    selectRouteParam,
    selectUrl
} from 'src/app/app.selectors';
import { selectUser } from 'src/app/pages/auth/auth.selectors';
import { AuthService } from 'src/app/pages/auth/services/auth.service';
import { BillingService } from 'src/app/pages/new-payment/services/billing.service';
import {
    createBilling,
    createBillingFail,
    createBillingSuccess,
    loadBilling,
    loadBillingFail,
    loadBillingSuccess,
    saveBilling,
    saveBillingFail,
    saveBillingSuccess
} from 'src/app/pages/new-payment/store/new-payment.actions';
import { Billing } from 'src/app/shared/interfaces/billing.interface';

@Injectable()
export class NewPaymentEffects {
    billingResolver$ = createEffect(() =>
        this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(
                this.store.pipe(select(selectUrl)),
                this.store.pipe(select(selectIsCreationMode)),
                this.store.pipe(select(selectRouteParam('billingId')))
            ),
            filter(
                ([_, url, __]) =>
                    url && url.includes('payment') && !url.includes('payments')
            ),
            map(([_, __, isCreationMode, id]) =>
                !isCreationMode ? loadBilling({ id: +id }) : createBilling()
            )
        )
    );

    createBilling$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createBilling),
            switchMap(() =>
                this.billingService.createBilling().pipe(
                    map((billing: Billing) =>
                        createBillingSuccess({ billing })
                    ),
                    catchError(error => of(createBillingFail({ error })))
                )
            )
        )
    );

    loadBilling$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadBilling),
            withLatestFrom(this.store.pipe(select(selectUser))),
            map(([{ id }, user]) =>
                user.billing.find(billing => billing.id === id)
            ),
            map(billing =>
                billing
                    ? loadBillingSuccess({ billing })
                    : loadBillingFail({ error: new Error('Billing not found') })
            )
        )
    );

    saveBilling$ = createEffect(() =>
        this.actions$.pipe(
            ofType(saveBilling),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([{ billing: payload }, user]) =>
                this.authService.saveBilling(user.id, payload).pipe(
                    map(billing => saveBillingSuccess({ billing })),
                    catchError(error => of(saveBillingFail({ error })))
                )
            )
        )
    );

    saveBillingSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(saveBillingSuccess),
                tap(() =>
                    this.navController.navigateForward(`/manage-payments`)
                )
            ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private authService: AuthService,
        private billingService: BillingService,
        private store: Store<AppState>
    ) {}
}
