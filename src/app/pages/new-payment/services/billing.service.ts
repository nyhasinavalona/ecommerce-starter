import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Billing } from 'src/app/shared/interfaces/billing.interface'

@Injectable()
export class BillingService {
    constructor() {}

    createBilling(): Observable<Billing> {
        return of({
            id: null,
            firstName: null,
            lastName: null,
            phone: null
        });
    }
}
