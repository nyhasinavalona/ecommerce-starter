import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
    @Input() product: Product;
    @Input() bool: boolean;

    constructor(private fun: UtilsService) {}

    ngOnInit() {}
}
