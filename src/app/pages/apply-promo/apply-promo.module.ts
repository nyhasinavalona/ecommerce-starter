import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ApplyPromoPage } from 'src/app/pages/apply-promo/apply-promo.page';

const routes: Routes = [
    {
        path: '',
        component: ApplyPromoPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ApplyPromoPage]
})
export class ApplyPromoPageModule {}
