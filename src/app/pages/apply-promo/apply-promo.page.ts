import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
    selector: 'app-apply-promo',
    templateUrl: './apply-promo.page.html',
    styleUrls: ['./apply-promo.page.scss']
})
export class ApplyPromoPage implements OnInit {
    constructor(private menuCtrl: MenuController) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(true, 'start');
    }
}
