import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, MenuController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import { selectAddresses } from 'src/app/pages/address-book/store/address-book.selectors';
import { checkout } from 'src/app/pages/cart/store/cart.actions';
import { selectBillings } from 'src/app/pages/manage-payments/store/payments.selectors';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { Billing } from 'src/app/shared/interfaces/billing.interface';
import { Order } from 'src/app/shared/models/order.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.page.html',
    styleUrls: ['./checkout.page.scss']
})
export class CheckoutPage implements OnInit {
    addNewDeliveryAddress = false;
    billings$: Observable<Billing[]>;
    addresses$: Observable<Address[]>;
    form: FormGroup;

    constructor(
        private menuCtrl: MenuController,
        private fun: UtilsService,
        private dataService: DataService,
        private alertController: AlertController,
        private store: Store<AppState>,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.billings$ = this.store.pipe(select(selectBillings));
        this.addresses$ = this.store.pipe(select(selectAddresses));
        this.form = this.initForm();
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    addPayment() {
        this.addNewDeliveryAddress = !this.addNewDeliveryAddress;
    }

    onSubmit() {
        this.store.dispatch(
            checkout({ order: new Order().deserialize(this.form.value) })
        );
    }

    async back() {
        const alert = await this.alertController.create({
            header: 'Are you sure?',
            message: 'Do you want to cancel entering your payment info?',
            buttons: [
                {
                    text: 'Yes',
                    cssClass: 'mycolor',
                    handler: blah => {
                        this.fun.back();
                    }
                },
                {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'mycolor',
                    handler: () => {}
                }
            ]
        });

        await alert.present();
    }

    private initForm(): FormGroup {
        return this.formBuilder.group({
            meanOfPayment: [null, Validators.required],
            deliveryAddress: [null, Validators.required]
        });
    }
}
