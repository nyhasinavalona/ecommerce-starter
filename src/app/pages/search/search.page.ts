import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { DataService } from 'src/app/data.service';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss']
})
export class SearchPage implements OnInit {
    trending = [];
    recent = [];

    constructor(
        private menuCtrl: MenuController,
        private fun: UtilsService,
        private dataService: DataService,
        private nav: NavController
    ) {
        this.trending = dataService.trending;
        this.recent = [];
    }

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'start');
        this.menuCtrl.enable(false, 'end');
    }
}
