import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-notificationssettings',
    templateUrl: './notifications-settings.page.html',
    styleUrls: ['./notifications-settings.page.scss']
})
export class NotificationsSettingsPage implements OnInit {
    constructor(private fun: UtilsService, private menuCtrl: MenuController) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }
}
