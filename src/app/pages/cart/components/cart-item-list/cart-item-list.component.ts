import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { IonList, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/data.service';
import { InfoModalPage } from 'src/app/pages/info-modal/info-modal.page';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { UtilsService } from 'src/app/utils.service';

const ALERT_OPTIONS = {
    header: 'Select Quantity',
    translucent: true
};

@Component({
    selector: 'app-cart-item-list',
    templateUrl: './cart-item-list.component.html',
    styleUrls: ['./cart-item-list.component.scss']
})
export class CartItemListComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('slidingList') slidingList: IonList;
    @Output() remove: EventEmitter<number> = new EventEmitter<number>();
    @Output() updateCart: EventEmitter<CartItem[]> = new EventEmitter<
        CartItem[]
    >();
    form: FormGroup;
    customAlertOptions: any = ALERT_OPTIONS;
    quantity: number[];
    @Input() items: CartItem[];
    @Input() itemsCost: number;
    @Input() shippingCost: number;
    private itemsSubscription: Subscription;

    constructor(
        private formBuilder: FormBuilder,
        private utilsService: UtilsService,
        private modalController: ModalController,
        private dataService: DataService
    ) {}

    get controls() {
        return this.form && (this.form.get('items') as FormArray).controls;
    }

    ngOnInit(): void {
        this.quantity = new Array(50).fill(0).map((v, i) => i + 1);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.items) {
            this.form = this.initForm(this.items);
            this.itemsSubscription = this.form
                .get('items')
                .valueChanges.subscribe(value => {
                    this.updateCart.emit(value);
                });
        }
    }

    ngOnDestroy(): void {
        if (this.itemsSubscription) {
            this.itemsSubscription.unsubscribe();
        }
    }

    browse() {
        this.utilsService.navigate('/home', false);
    }

    async openModal(b) {
        let modal;
        if (b) {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.dataService.terms_of_use,
                    title: 'Terms of Use'
                }
            });
        } else {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.dataService.privacy_policy,
                    title: 'Privacy Policy'
                }
            });
        }
        return await modal.present();
    }

    private initForm(items: CartItem[]) {
        return this.formBuilder.group({
            items: this.initCartItems(items),
            code: [null]
        });
    }

    private initCartItems(items: CartItem[]): FormArray {
        return this.formBuilder.array(
            items.map(item =>
                this.formBuilder.group({
                    product: item.product,
                    quantity: item.quantity || 1
                })
            )
        );
    }
}
