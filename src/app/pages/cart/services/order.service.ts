import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from 'src/app/shared/models/order.model';
import { BaseService } from 'src/app/shared/services/base.service';

@Injectable()
export class OrderService extends BaseService<Order> {
    constructor(http: HttpClient) {
        super(http, Order, 'order');
    }
}
