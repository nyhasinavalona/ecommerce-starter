import { Injectable } from '@angular/core';
import { ORDER_MOCKS } from 'src/app/pages/cart/services/order.mocks';
import { Order } from 'src/app/shared/models/order.model';
import { BaseMockService } from 'src/app/shared/services/base-mock.service';

@Injectable()
export class OrderMockService extends BaseMockService<Order> {
    constructor() {
        super(Order, ORDER_MOCKS);
    }
}
