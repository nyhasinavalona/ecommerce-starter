import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CartPage } from 'src/app/pages/cart/cart.page';
import { CartItemListComponent } from 'src/app/pages/cart/components/cart-item-list/cart-item-list.component';

const routes: Routes = [
    {
        path: '',
        component: CartPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CartPage, CartItemListComponent]
})
export class CartPageModule {}
