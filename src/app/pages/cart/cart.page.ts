import { Component, OnInit, ViewChild } from '@angular/core';
import {
    AlertController,
    IonList,
    MenuController,
    ModalController,
    NavController
} from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import {
    removeItem,
    updateCartItems
} from 'src/app/pages/cart/store/cart.actions';
import {
    selectCartItemCost,
    selectCartItems,
    selectShippingCost,
    selectShowCartSummary
} from 'src/app/pages/cart/store/cart.selectors';
import { InfoModalPage } from 'src/app/pages/info-modal/info-modal.page';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.page.html',
    styleUrls: ['./cart.page.scss']
})
export class CartPage implements OnInit {
    code: string = null;
    show = true;
    show$: Observable<boolean>;
    items$: Observable<CartItem[]>;
    itemsCost$: Observable<number>;
    shippingCost$: Observable<number>;

    constructor(
        private menuCtrl: MenuController,
        public dataService: DataService,
        public fun: UtilsService,
        private modalController: ModalController,
        private nav: NavController,
        public alertController: AlertController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.items$ = this.store.pipe(select(selectCartItems));
        this.itemsCost$ = this.store.pipe(select(selectCartItemCost));
        this.shippingCost$ = this.store.pipe(select(selectShippingCost));
        this.show$ = this.store.pipe(select(selectShowCartSummary));
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    browse() {
        this.fun.navigate('/home', false);
    }

    remove(j) {}

    onUpdateCarItems(items: CartItem[]) {
        this.store.dispatch(updateCartItems({ items }));
    }

    onRemove(id: number) {
        this.store.dispatch(removeItem({ id }));
    }
}
