import { createSelector } from '@ngrx/store';
import { selectCartState } from 'src/app/app.selectors';

export const selectCartItems = createSelector(
    selectCartState,
    state => state.items
);
export const selectShowCartSummary = createSelector(
    selectCartItems,
    items => !!items.length
);
export const selectCartItemCost = createSelector(selectCartItems, items =>
    items.reduce((a, c) => {
        if (c.product.offer) {
            const price =
                c.product.price - (c.product.price * c.product.discount) / 100;
            return a + price * c.quantity;
        }
        return a + c.product.price * c.quantity;
    }, 0)
);
export const selectShippingCost = createSelector(selectCartItems, items =>
    items.reduce((a, c) => a + c.product.shipping * c.quantity, 0)
);
