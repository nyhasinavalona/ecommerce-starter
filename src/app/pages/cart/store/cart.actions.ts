import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { Order } from 'src/app/shared/models/order.model';

export const addItem = createAction('ADD_ITEM');
export const addItemSuccess = createAction(
    'ADD_ITEM_SUCCESS',
    props<{ item: CartItem }>()
);
export const addItemFail = createAction(
    'ADD_ITEM_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const removeItem = createAction('REMOVE_ITEM', props<{ id: number }>());
export const removeItemSuccess = createAction('REMOVE_ITEM_SUCCESS');
export const removeItemFail = createAction(
    'REMOVE_ITEM_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const updateCartItems = createAction(
    'UPDATE_CART_ITEMS',
    props<{ items: CartItem[] }>()
);
export const updateCartItemsSuccess = createAction('UPDATE_CART_ITEMS_SUCCESS');
export const updateCartItemsFail = createAction(
    'UPDATE_CART_ITEMS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const checkout = createAction('CHECKOUT', props<{ order: Order }>());
export const checkoutSuccess = createAction(
    'CHECKOUT_SUCCESS',
    props<{ order: Order }>()
);
export const checkoutFail = createAction(
    'CHECKOUT_FAIL',
    props<{ error: HttpErrorResponse }>()
);
