import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import {
    addItemFail,
    addItemSuccess,
    checkoutSuccess,
    removeItem,
    removeItemFail,
    updateCartItems,
    updateCartItemsFail
} from 'src/app/pages/cart/store/cart.actions';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { Order } from 'src/app/shared/models/order.model';

export interface CartState {
    items: CartItem[];
    orders: Order[];
    order: Order;
    error: HttpErrorResponse;
}

export const initialState: CartState = {
    items: [],
    orders: [],
    order: null,
    error: null
};

const reducer = createReducer(
    initialState,
    on(addItemSuccess, (state, { item }) => ({
        ...state,
        items: [...state.items, item]
    })),
    on(addItemFail, (state, { error }) => ({ ...state, error })),
    on(removeItem, (state, { id }) => ({
        ...state,
        items: [...state.items.filter((_, i) => +i !== +id)]
    })),
    on(removeItemFail, (state, { error }) => ({ ...state, error })),
    on(addItemFail, (state, { error }) => ({ ...state, error })),
    on(updateCartItems, (state, { items }) => ({
        ...state,
        items: [...items]
    })),
    on(updateCartItemsFail, (state, { error }) => ({ ...state, error })),
    on(checkoutSuccess, (state, { order }) => ({ ...state, order, items: [] }))
);

export function cartReducer(state, action) {
    return reducer(state, action);
}
