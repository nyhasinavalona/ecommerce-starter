import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
    catchError,
    map,
    switchMap,
    tap,
    withLatestFrom
} from 'rxjs/operators';
import { AppState } from 'src/app/app.reducers';
import { selectUser } from 'src/app/pages/auth/auth.selectors';
import { OrderService } from 'src/app/pages/cart/services/order.service';
import {
    addItem,
    addItemSuccess,
    checkout,
    checkoutFail,
    checkoutSuccess
} from 'src/app/pages/cart/store/cart.actions';
import {
    selectCartItemCost,
    selectCartItems,
    selectShippingCost
} from 'src/app/pages/cart/store/cart.selectors';
import { selectProduct } from 'src/app/pages/product-details/store/product-details.selectors';
import { Order } from 'src/app/shared/models/order.model';
import swal from 'sweetalert';

@Injectable()
export class CartEffects {
    addItem$ = createEffect(() =>
        this.actions$.pipe(
            ofType(addItem),
            withLatestFrom(this.store.pipe(select(selectProduct))),
            map(([_, product]) => {
                return addItemSuccess({ item: { product, quantity: 1 } });
            })
        )
    );

    addItemSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(addItemSuccess),
                tap(() => this.navController.navigateForward(`/cart`))
            ),
        { dispatch: false }
    );

    checkout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(checkout),
            withLatestFrom(
                this.store.pipe(select(selectUser)),
                this.store.pipe(select(selectCartItems)),
                this.store.pipe(select(selectCartItemCost)),
                this.store.pipe(select(selectShippingCost))
            ),
            map(([{ order }, user, items, cost, shippingCost]) =>
                new Order().deserialize({
                    ...order.serialize(),
                    user: user.id,
                    items,
                    cost,
                    shippingCost
                })
            ),
            switchMap(order =>
                this.orderService.save(order).pipe(
                    map(payload => checkoutSuccess({ order: payload })),
                    catchError(error => of(checkoutFail({ error })))
                )
            )
        )
    );

    checkoutSuccess = createEffect(
        () =>
            this.actions$.pipe(
                ofType(checkoutSuccess),
                tap(({ order }) => {
                    swal(
                        'Formidable',
                        `Vous venez juste de commander ${order.items.length} article(s)`,
                        'success'
                    );
                    this.navController.navigateForward('/home');
                })
            ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private store: Store<AppState>,
        private orderService: OrderService
    ) {}
}
