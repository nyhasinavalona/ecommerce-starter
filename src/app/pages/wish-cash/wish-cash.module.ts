import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WishCashPage } from 'src/app/pages/wish-cash/wish-cash.page';

const routes: Routes = [
    {
        path: '',
        component: WishCashPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [WishCashPage]
})
export class WishCashPageModule {}
