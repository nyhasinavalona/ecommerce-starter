import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';
import { DataService } from 'src/app/data.service';
import { MenuController } from '@ionic/angular';

@Component({
    selector: 'app-support',
    templateUrl: './support.page.html',
    styleUrls: ['./support.page.scss']
})
export class SupportPage implements OnInit {
    conversation = [
        {
            text: 'Bonjour, comment puis-je vous aidez ?',
            sender: 0,
            image: 'assets/images/sg.jpg'
        },
    ];

    input = '';

    constructor(
        private fun: UtilsService,
        private dataService: DataService,
        private menuCtrl: MenuController
    ) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(true, 'start');
    }

    send() {
        if (this.input != '') {
            this.conversation.push({
                text: this.input,
                sender: 1,
                image: 'https://tanaytoshniwal.me/assets/images/icon_favo.jpg'
            });
            this.input = '';
        }
    }
}
