import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigationAction } from '@ngrx/router-store';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
    catchError,
    filter,
    map,
    switchMap,
    tap,
    withLatestFrom
} from 'rxjs/operators';
import { AppState } from 'src/app/app.reducers';
import {
    selectIsCreationMode,
    selectRouteParam,
    selectUrl
} from 'src/app/app.selectors';
import { selectUser } from 'src/app/pages/auth/auth.selectors';
import { AuthService } from 'src/app/pages/auth/services/auth.service';
import { AddressService } from 'src/app/pages/new-address/services/address.service';
import {
    createAddress,
    createAddressFail,
    createAddressSuccess,
    loadAddress,
    loadAddressFail,
    loadAddressSuccess,
    saveAddress,
    saveAddressFail,
    saveAddressSuccess
} from 'src/app/pages/new-address/store/new-address.actions';
import { Address } from 'src/app/shared/interfaces/address.interface';

@Injectable()
export class NewAddressEffects {
    addressResolver$ = createEffect(() =>
        this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(
                this.store.pipe(select(selectUrl)),
                this.store.pipe(select(selectIsCreationMode)),
                this.store.pipe(select(selectRouteParam('addressId')))
            ),
            filter(
                ([_, url, __]) =>
                    url && url.includes('address') && !url.includes('book')
            ),
            map(([_, __, isCreationMode, id]) =>
                !isCreationMode ? loadAddress({ id: +id }) : createAddress()
            )
        )
    );

    createAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(createAddress),
            switchMap(() =>
                this.addressService.createAddress().pipe(
                    map((address: Address) =>
                        createAddressSuccess({ address })
                    ),
                    catchError(error => of(createAddressFail({ error })))
                )
            )
        )
    );

    loadAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadAddress),
            withLatestFrom(this.store.pipe(select(selectUser))),
            map(([{ id }, user]) =>
                user.address.find(address => address.id === id)
            ),
            map(address =>
                address
                    ? loadAddressSuccess({ address })
                    : loadAddressFail({ error: new Error('Address not found') })
            )
        )
    );

    saveAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(saveAddress),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([{ address: payload }, user]) =>
                this.authService.saveAddress(user.id, payload).pipe(
                    map(address => saveAddressSuccess({ address })),
                    catchError(error => of(saveAddressFail({ error })))
                )
            )
        )
    );

    saveAddressSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(saveAddressSuccess),
                tap(() => this.navController.navigateForward(`/address-book`))
            ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private authService: AuthService,
        private addressService: AddressService,
        private store: Store<AppState>
    ) {}
}
