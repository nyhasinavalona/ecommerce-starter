import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { Error } from 'tslint/lib/error';

export const loadAddress = createAction(
    'LOAD_ADDRESS',
    props<{ id: number }>()
);
export const loadAddressFail = createAction(
    'LOAD_ADDRESS_FAIL',
    props<{ error: Error }>()
);
export const loadAddressSuccess = createAction(
    'LOAD_ADDRESS_SUCCESS',
    props<{ address: Address }>()
);
export const createAddress = createAction('CREATE_ADDRESS');
export const createAddressFail = createAction(
    'CREATE_ADDRESS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const createAddressSuccess = createAction(
    'CREATE_ADDRESS_SUCCESS',
    props<{ address: Address }>()
);
export const saveAddress = createAction(
    'SAVE_ADDRESS',
    props<{ address: Address }>()
);
export const saveAddressFail = createAction(
    'SAVE_ADDRESS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const saveAddressSuccess = createAction(
    'SAVE_ADDRESS_SUCCESS',
    props<{ address: Address }>()
);
