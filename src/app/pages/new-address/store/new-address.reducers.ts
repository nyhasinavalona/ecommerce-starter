import { createReducer, on } from '@ngrx/store';
import {
    createAddressSuccess,
    loadAddressSuccess
} from 'src/app/pages/new-address/store/new-address.actions';
import { Address } from 'src/app/shared/interfaces/address.interface'
export interface AddressState {
    address: Address;
}

export const initialState: AddressState = {
    address: null
};

const reducer = createReducer(
    initialState,
    on(createAddressSuccess, (state, { address }) => ({ ...state, address })),
    on(loadAddressSuccess, (state, { address }) => ({ ...state, address }))
);

export function addressReducer(state, action) {
    return reducer(state, action);
}
