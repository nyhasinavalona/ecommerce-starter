import { createSelector } from '@ngrx/store';
import {
    selectAddressState,
    selectProductDetailsState
} from 'src/app/app.selectors';

export const selectAddress = createSelector(selectAddressState, state => state.address);

export const selectRelatedProducts = createSelector(
    selectProductDetailsState,
    state => state.relatedProducts
);
