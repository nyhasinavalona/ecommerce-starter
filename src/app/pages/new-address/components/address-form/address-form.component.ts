import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core'
import { DefaultKeyValueDiffer } from '@angular/core/src/change_detection/differs/default_keyvalue_differ'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { CITIES, REGIONS } from 'src/app/pages/new-address/address.constants'
import { Address } from 'src/app/shared/interfaces/address.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-address-form',
    templateUrl: './address-form.component.html',
    styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnChanges {
    form: FormGroup;
    cities = CITIES;
    regions = REGIONS;
    @Input() address: Address;
    @Output() save: EventEmitter<Address> = new EventEmitter<Address>();

    constructor(
        private formBuilder: FormBuilder,
        private utilsService: UtilsService
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.address) {
            this.form = this.initForm(this.address);
        }
    }

    onSubmit() {
        if (this.form.valid) {
            this.save.emit(this.form.value);
        } else {
            this.utilsService.presentToast('Des champs sont invalides.', true, 'top', 1500);
        }
    }

    private initForm(address: Address) {
        return this.formBuilder.group({
            id: [address.id],
            firstName: [address.firstName],
            lastName: [address.lastName],
            firstAddressLine: [address.firstAddressLine, Validators.required],
            secondAddressLine: [address.secondAddressLine],
            city: [address.city, Validators.required],
            region: [address.region],
            zipCode: [address.zipCode, Validators.required],
            phone: [address.phone]
        });
    }
}
