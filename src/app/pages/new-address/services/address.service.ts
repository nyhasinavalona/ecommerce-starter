import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Address } from 'src/app/shared/interfaces/address.interface';

@Injectable()
export class AddressService {
    constructor() {}

    createAddress(): Observable<Address> {
        return of({
            id: null,
            firstName: null,
            lastName: null,
            phone: null,
            zipCode: null,
            city: null,
            region: null,
            secondAddressLine: null,
            firstAddressLine: null,
            street: null
        });
    }
}
