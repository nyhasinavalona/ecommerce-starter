import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { HomeMockService } from 'src/app/pages/home/services/home-mock.service';
import { HomeService } from 'src/app/pages/home/services/home.service';
import { AddressFormComponent } from 'src/app/pages/new-address/components/address-form/address-form.component';
import { NewAddressPage } from 'src/app/pages/new-address/new-address.page';
import { AddressService } from 'src/app/pages/new-address/services/address.service';
import { NewAddressEffects } from 'src/app/pages/new-address/store/new-address.effects';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    {
        path: 'edit/:addressId',
        component: NewAddressPage
    },
    {
        path: 'new',
        component: NewAddressPage
    },
    {
        path: '',
        redirectTo: 'new',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        EffectsModule.forFeature([NewAddressEffects])
    ],
    declarations: [NewAddressPage, AddressFormComponent],
    providers: [
        AddressService,
        {
            provide: HomeService,
            useClass: environment.mock.home ? HomeMockService : HomeService
        }
    ]
})
export class NewAddressPageModule {}
