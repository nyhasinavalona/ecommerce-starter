import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { selectIsCreationMode } from 'src/app/app.selectors';
import { selectAddress } from 'src/app/pages/new-address/store/new-address.selectors';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { MenuController } from '@ionic/angular';
import { saveAddress } from './store/new-address.actions';

@Component({
    selector: 'app-new-address',
    templateUrl: './new-address.page.html',
    styleUrls: ['./new-address.page.scss']
})
export class NewAddressPage implements OnInit {
    address$: Observable<Address>;
    creationMode$: Observable<boolean>;
    constructor(
        private menuCtrl: MenuController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.address$ = this.store.pipe(select(selectAddress));
        this.creationMode$ = this.store.pipe(select(selectIsCreationMode));
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    onSave(address: Address) {
        this.store.dispatch(saveAddress({ address }));
    }
}
