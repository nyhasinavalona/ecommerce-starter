import { HomeTab } from 'src/app/shared/interfaces/home-tab.interface';

export const SLIDE_OPTS = {
    effect: 'flip',
    zoom: false
};

export enum ProductCategory {
    BASIC_NEEDS = 'BASIC_NEEDS',
    RICE = 'RICE',
    DRINKS = 'DRINKS',
    ALCOHOL = 'ALCOHOL',
    COOKIE = 'COOKIE',
    VARIOUS = 'VARIOUS'
}

export const PRODUCT_CATEGORY_LABELS = {
    [ProductCategory.BASIC_NEEDS]: 'P. Première Nécessité',
    [ProductCategory.RICE]: 'Riz',
    [ProductCategory.DRINKS]: 'Boissons',
    [ProductCategory.ALCOHOL]: 'Boissons Alcoolique',
    [ProductCategory.COOKIE]: 'Biscuits',
    [ProductCategory.VARIOUS]: 'Autres'
};

export const CATEGORIES: HomeTab[] = Object.values(
    ProductCategory
).map(value => ({ title: value }));
