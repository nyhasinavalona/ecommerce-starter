import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides, MenuController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import {
    CATEGORIES,
    PRODUCT_CATEGORY_LABELS,
    ProductCategory,
    SLIDE_OPTS
} from 'src/app/pages/home/home.constants';
import { loadProducts } from 'src/app/pages/home/store/home.actions';
import { selectProducts } from 'src/app/pages/home/store/home.selectors';
import { HomeTab } from 'src/app/shared/interfaces/home-tab.interface';
import { Product } from 'src/app/shared/models/product.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
    @ViewChild('categorySlides') slides: IonSlides;
    categories: HomeTab[] = [];
    productCategoryLabels = PRODUCT_CATEGORY_LABELS;
    segment = null;
    index = 0;
    products$: Observable<Product[]>;
    slideOpts = SLIDE_OPTS;

    constructor(
        private activatedRoute: ActivatedRoute,
        private menuCtrl: MenuController,
        private utilsService: UtilsService,
        private store: Store<AppState>
    ) {}

    ngOnInit(): void {
        this.categories = CATEGORIES;
        this.store.dispatch(
            loadProducts({
                criteria: { category: Object.values(ProductCategory)[0] }
            })
        );
        const id = this.activatedRoute.snapshot.paramMap.get('id');
        this.segment = id
            ? this.categories[+id].title
            : this.categories[0].title;
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'start');
        this.menuCtrl.enable(true, 'end');
        this.products$ = this.store.pipe(select(selectProducts));
    }

    onSegmentChange(event) {
        this.segment = event.detail.value;
        this.store.dispatch(
            loadProducts({ criteria: { category: this.segment } })
        );
    }

    drag() {
        let distanceToScroll = 0;
        for (const index in this.categories) {
            if (+index < this.index) {
                distanceToScroll =
                    distanceToScroll +
                    document.getElementById('seg_' + index).offsetWidth +
                    24;
            }
        }
        document.getElementById('dag').scrollLeft = distanceToScroll;
    }

    preventDefault(e) {
        e.preventDefault();
    }

    async change() {
        await this.slides.getActiveIndex().then(data => (this.index = data));
        this.segment = this.categories[this.index].title;
        this.drag();
    }

    openSide() {
        this.menuCtrl.toggle('end');
    }

    async onSelect(i) {
        await this.slides.slideTo(i);
    }
}
