import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { HomePage } from 'src/app/pages/home/home.page';
import { HomeMockService } from 'src/app/pages/home/services/home-mock.service';
import { HomeService } from 'src/app/pages/home/services/home.service';
import { HomeEffects } from 'src/app/pages/home/store/home.effects';
import { ProductListPage } from 'src/app/pages/home/components/product-list/product-list.page';
import { environment } from 'src/environments/environment';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: ':id',
                component: HomePage
            },
            {
                path: '',
                redirectTo: '1',
                pathMatch: 'full'
            }
        ]),
        EffectsModule.forFeature([HomeEffects])
    ],
    declarations: [HomePage, ProductListPage],
    providers: [
        {
            provide: HomeService,
            useClass: environment.mock.home ? HomeMockService : HomeService
        }
    ],
    entryComponents: [ProductListPage]
})
export class HomePageModule {}
