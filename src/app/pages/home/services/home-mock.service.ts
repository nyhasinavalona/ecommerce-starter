import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ProductCriteria } from 'src/app/pages/home/interfaces/product-criteria.interface';
import { PRODUCT_MOCKS } from 'src/app/pages/home/services/home.mocks';
import { Product } from 'src/app/shared/models/product.model';
import { BaseMockService } from 'src/app/shared/services/base-mock.service';

@Injectable()
export class HomeMockService extends BaseMockService<Product> {
    constructor() {
        super(Product, PRODUCT_MOCKS);
    }

    loadAll(criteria?: ProductCriteria): Observable<Product[]> {
        if (criteria && criteria.category) {
            return of(
                this.mocks.filter(item => item.category === criteria.category)
            );
        }
        return of(this.mocks);
    }
}
