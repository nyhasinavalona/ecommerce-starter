import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from 'src/app/shared/models/product.model';
import { BaseService } from 'src/app/shared/services/base.service';

@Injectable()
export class HomeService extends BaseService<Product> {
    constructor(http: HttpClient) {
        super(http, Product, ``);
    }
}
