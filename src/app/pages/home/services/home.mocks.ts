/* tslint:disable:max-line-length */
import { ProductCategory } from 'src/app/pages/home/home.constants';
import { Product } from 'src/app/shared/models/product.model';

export const PRODUCT_MOCKS: Product[] = [
    {
        name: `Bianca Top`,
        price: 12800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/1.jpg`,
            `/assets/images/products/1_1.jpg`,
            `/assets/images/products/1_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 1200,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/1.jpg`,
                    `/assets/images/products/1_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/1_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/1.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/1.jpg`,
                    `/assets/images/products/1_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/1_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/1.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Scarpetta Dress`,
        price: 19800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/2.jpg`,
            `/assets/images/products/2_1.jpg`,
            `/assets/images/products/2_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 400,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/2.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/2_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/2.jpg`,
                    `/assets/images/products/2_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/2.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Misty Dress`,
        price: 21800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/3.jpg`,
            `/assets/images/products/3_1.jpg`,
            `/assets/images/products/3_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 365,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/3.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/3.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/3_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/3.jpg`,
                    `/assets/images/products/3_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/3.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/3.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Blanch Dress`,
        price: 24800,
        discount: 20,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/4.jpg`,
            `/assets/images/products/4_1.jpg`,
            `/assets/images/products/4_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 1200,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/4_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/4.jpg`,
                    `/assets/images/products/4_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Scarlett Dress`,
        price: 21800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/5.jpg`,
            `/assets/images/products/5_1.jpg`,
            `/assets/images/products/5_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 234,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/5_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/5.jpg`,
                    `/assets/images/products/5_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Morrisson Dress`,
        price: 12800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/6.jpg`,
            `/assets/images/products/6_1.jpg`,
            `/assets/images/products/6_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 567,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/6.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/6.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/6_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/6.jpg`,
                    `/assets/images/products/6_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/6.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/6.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Morrisson Dress`,
        price: 12800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/7.jpg`,
            `/assets/images/products/7_1.jpg`,
            `/assets/images/products/7_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 137,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/7_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/7.jpg`,
                    `/assets/images/products/7_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Smith Dress`,
        price: 9800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/8.jpg`,
            `/assets/images/products/8_1.jpg`,
            `/assets/images/products/8_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 236,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/8.jpg`,
                    `/assets/images/products/8_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Nicola Dress`,
        price: 27800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/9.jpg`,
            `/assets/images/products/9_1.jpg`,
            `/assets/images/products/9_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 982,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/9.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/9.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/9_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/9.jpg`,
                    `/assets/images/products/9_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/9.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/9.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Smith Dress`,
        price: 9800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/10.jpg`,
            `/assets/images/products/10_1.jpg`,
            `/assets/images/products/10_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 214,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/10.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/10.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/10_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/10.jpg`,
                    `/assets/images/products/10_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/10.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/10.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Kinsley Dress`,
        price: 19800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/11.jpg`,
            `/assets/images/products/11_1.jpg`,
            `/assets/images/products/11_2.jpg`,
            `/assets/images/products/11_3.jpg`,
            `/assets/images/products/11_4.jpg`,
            `/assets/images/products/11_5.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 434,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/11.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/11.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/11.jpg`,
                    `/assets/images/products/11_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/11_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/1.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Bianca Top`,
        price: 12800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/1.jpg`,
            `/assets/images/products/1_1.jpg`,
            `/assets/images/products/1_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 1200,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/1.jpg`,
                    `/assets/images/products/1_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/1_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/1.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/1.jpg`,
                    `/assets/images/products/1_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/1_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/1.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Scarpetta Dress`,
        price: 19800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/2.jpg`,
            `/assets/images/products/2_1.jpg`,
            `/assets/images/products/2_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 400,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/2.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/2_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/2.jpg`,
                    `/assets/images/products/2_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/2.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Misty Dress`,
        price: 21800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/3.jpg`,
            `/assets/images/products/3_1.jpg`,
            `/assets/images/products/3_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 365,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/3.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/3.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/3_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/3.jpg`,
                    `/assets/images/products/3_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/3.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/3.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Blanch Dress`,
        price: 24800,
        discount: 20,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/4.jpg`,
            `/assets/images/products/4_1.jpg`,
            `/assets/images/products/4_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 1200,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/4_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/4.jpg`,
                    `/assets/images/products/4_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Scarlett Dress`,
        price: 21800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/5.jpg`,
            `/assets/images/products/5_1.jpg`,
            `/assets/images/products/5_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 234,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/5_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/5.jpg`,
                    `/assets/images/products/5_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Morrisson Dress`,
        price: 12800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/7.jpg`,
            `/assets/images/products/7_1.jpg`,
            `/assets/images/products/7_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 137,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/7_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/7.jpg`,
                    `/assets/images/products/7_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Smith Dress`,
        price: 9800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/8.jpg`,
            `/assets/images/products/8_1.jpg`,
            `/assets/images/products/8_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 236,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/8.jpg`,
                    `/assets/images/products/8_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Nicola Dress`,
        price: 27800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/9.jpg`,
            `/assets/images/products/9_1.jpg`,
            `/assets/images/products/9_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 982,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/9.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/9.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/9_2.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/9.jpg`,
                    `/assets/images/products/9_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/9.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/9.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Smith Dress`,
        price: 9800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/10.jpg`,
            `/assets/images/products/10_1.jpg`,
            `/assets/images/products/10_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 214,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/10.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/10.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/10_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/10.jpg`,
                    `/assets/images/products/10_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/10.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/10.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Blanch Dress`,
        price: 24800,
        discount: 20,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/4.jpg`,
            `/assets/images/products/4_1.jpg`,
            `/assets/images/products/4_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 1200,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/4_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/4.jpg`,
                    `/assets/images/products/4_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/4.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/4.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Scarlett Dress`,
        price: 21800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/5.jpg`,
            `/assets/images/products/5_1.jpg`,
            `/assets/images/products/5_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 234,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/5_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/5.jpg`,
                    `/assets/images/products/5_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/5.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/5.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Morrisson Dress`,
        price: 12800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/6.jpg`,
            `/assets/images/products/6_1.jpg`,
            `/assets/images/products/6_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 567,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/6.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/6.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/6_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/6.jpg`,
                    `/assets/images/products/6_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/6.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/6.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Morrisson Dress`,
        price: 12800,
        discount: 80,
        offer: true,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/7.jpg`,
            `/assets/images/products/7_1.jpg`,
            `/assets/images/products/7_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 137,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/7_1.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/7.jpg`,
                    `/assets/images/products/7_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/7.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/7.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    },
    {
        name: `Smith Dress`,
        price: 9800,
        discount: 80,
        offer: false,
        stock: 69,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        image: [
            `/assets/images/products/8.jpg`,
            `/assets/images/products/8_1.jpg`,
            `/assets/images/products/8_2.jpg`
        ],
        ratingCount: 11,
        storeRatingCount: 11,
        currency: 'Ar',
        bought: 236,
        size: `M`,
        color: `Black`,
        shipping: 25000,
        rating: 4,
        storeRating: 18090,
        storeRate: 3,
        soldBy: `Groupe Vidzar`,
        specs: `Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
        reviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        storeReviews: [
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [
                    `/assets/images/products/8.jpg`,
                    `/assets/images/products/8_1.jpg`
                ]
            },
            {
                image: `/assets/images/products/8.jpg`,
                name: `sample`,
                comment: `This is an amazing dress and totally out of budget. Need to sell my only kidney to purchase this one as other kidney is already sold for my iphone...lol`,
                rating: 5,
                images: [`/assets/images/products/8.jpg`]
            }
        ],
        sizing: { small: 10, okay: 80, large: 5 },
        buyerGuarantee: `Return all products within 30 days of delivery if they are not up to your satisfaction`,
        sponsored: []
    }
].map((item, i) => {
    const categories = Object.values(ProductCategory);
    const id = Math.abs(Math.floor(Math.random() * (categories.length + 1)));
    return new Product().deserialize({
        ...item,
        id: i,
        category: categories[id]
    });
});
