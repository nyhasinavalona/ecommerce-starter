import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { ProductCriteria } from 'src/app/pages/home/interfaces/product-criteria.interface';
import {
    loadProducts,
    loadProductsFail,
    loadProductsSuccess
} from 'src/app/pages/home/store/home.actions';
import { CartItem } from 'src/app/shared/interfaces/cart-item.interface';
import { Product } from 'src/app/shared/models/product.model';

export interface HomeState {
    products: Product[];
    cart: CartItem[];
    criteria: ProductCriteria;
    error: HttpErrorResponse;
}

export const initialState: HomeState = {
    products: [],
    criteria: null,
    cart: [],
    error: null
};

const reducer = createReducer(
    initialState,
    on(loadProducts, (state, { criteria }) => ({
        ...state,
        criteria,
        products: []
    })),
    on(loadProductsFail, (state, { error }) => ({ ...state, error })),
    on(loadProductsSuccess, (state, { products }) => ({ ...state, products }))
);

export function homeReducer(state, action) {
    return reducer(state, action);
}
