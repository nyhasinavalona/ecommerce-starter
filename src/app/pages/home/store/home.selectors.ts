import { createSelector } from '@ngrx/store';
import { selectHomeState } from 'src/app/app.selectors';

export const selectProducts = createSelector(
    selectHomeState,
    state => state.products
);

export const selectCart = createSelector(selectHomeState, state => state.cart);
