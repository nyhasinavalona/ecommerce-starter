import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { HomeService } from 'src/app/pages/home/services/home.service';
import {
    loadProducts,
    loadProductsFail,
    loadProductsSuccess
} from 'src/app/pages/home/store/home.actions';

@Injectable()
export class HomeEffects {
    loadProducts$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadProducts),
            switchMap(({ criteria }) =>
                this.homeService.loadAll(criteria).pipe(
                    map(products => loadProductsSuccess({ products })),
                    catchError(error => of(loadProductsFail({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private homeService: HomeService
    ) {}
}
