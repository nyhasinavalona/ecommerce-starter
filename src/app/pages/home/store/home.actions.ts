import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ProductCriteria } from 'src/app/pages/home/interfaces/product-criteria.interface';
import { Product } from 'src/app/shared/models/product.model';

export const loadProducts = createAction(
    'LOAD_PRODUCTS',
    props<{ criteria?: ProductCriteria }>()
);
export const loadProductsFail = createAction(
    'LOAD_PRODUCTS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const loadProductsSuccess = createAction(
    'LOAD_PRODUCTS_SUCCESS',
    props<{ products: Product[] }>()
);
