import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Product } from 'src/app/shared/models/product.model';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.page.html',
    styleUrls: ['./product-list.page.scss']
})
export class ProductListPage implements OnInit {
    @Input() items: Product[];

    constructor(
        private utilsService: UtilsService,
        private nav: NavController
    ) {}

    ngOnInit() {}

    open(product: Product) {
        this.nav.navigateForward(`/product-details/${product.id}`);
    }
}
