import { ProductCategory } from 'src/app/pages/home/home.constants';
import { Criteria } from 'src/app/shared/interfaces/criteria.interface';
import { Product } from 'src/app/shared/models/product.model';

export interface ProductCriteria extends Criteria<Product> {
    category: ProductCategory;
}
