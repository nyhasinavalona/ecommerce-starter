import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-accountsettings',
    templateUrl: './account-settings.page.html',
    styleUrls: ['./account-settings.page.scss']
})
export class AccountSettingsPage implements OnInit {
    items = [
        { name: 'Country/Region', url: 'country' },
        { name: 'Update Profile', url: '' },
        { name: 'Change Email Address', url: 'change-email' },
        { name: 'Change Password', url: 'change-password' },
        { name: 'Deactivate Account', url: 'deactivate' }
    ];

    constructor(
        public fun: UtilsService,
        private menuCtrl: MenuController,
        private alertController: AlertController,
        private nav: NavController
    ) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }

    async open(i) {
        if (this.items[i].url == 'deactivate') {
            const alert = await this.alertController.create({
                header: 'Are you sure?',
                message: 'Do you really want to deactivate your account?',
                buttons: [
                    {
                        text: 'Yes',
                        cssClass: 'mycolor',
                        handler: blah => {
                            this.nav.navigateRoot('/auth');
                        }
                    },
                    {
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'mycolor',
                        handler: () => {}
                    }
                ]
            });

            await alert.present();
        } else {
            this.fun.navigate(this.items[i].url, false);
        }
    }
}
