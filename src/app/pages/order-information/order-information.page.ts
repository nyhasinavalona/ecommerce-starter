/**
 * Shoppr - E-commerce app starter Ionic 4(https://www.enappd.com)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source .
 *
 */
import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Orders } from 'src/app/data.service';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-orderinfo',
    templateUrl: './order-information.page.html',
    styleUrls: ['./order-information.page.scss']
})
export class OrderInformationPage implements OnInit {
    order: Orders;

    constructor(
        private modalController: ModalController,
        private params: NavParams,
        private fun: UtilsService
    ) {
        this.order = params.get('value');
    }

    ngOnInit() {}

    dismiss() {
        this.modalController.dismiss();
    }
}
