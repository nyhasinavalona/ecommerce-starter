import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular'
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import { selectBillings } from 'src/app/pages/manage-payments/store/payments.selectors';
import { Billing } from 'src/app/shared/interfaces/billing.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-managepayments',
    templateUrl: './manage-payments.page.html',
    styleUrls: ['./manage-payments.page.scss']
})
export class ManagePaymentsPage implements OnInit {
    billings$: Observable<Billing[]>;

    constructor(
        private dataService: DataService,
        private navController: NavController,
        private menuCtrl: MenuController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.billings$ = this.store.pipe(select(selectBillings));
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    addNew() {
        this.navController.navigateForward(`/payment/new`);
    }
}
