import { createSelector } from '@ngrx/store'
import { selectPaymentsState } from 'src/app/app.selectors'

export const selectBillings = createSelector(
    selectPaymentsState,
    state => state.billings
);
