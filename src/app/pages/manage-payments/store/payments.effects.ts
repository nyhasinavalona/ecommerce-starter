import { Injectable } from '@angular/core'
import { NavController } from '@ionic/angular'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { routerNavigationAction } from '@ngrx/router-store'
import { select, Store } from '@ngrx/store'
import { of } from 'rxjs'
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators'
import { AppState } from 'src/app/app.reducers'
import { selectUrl } from 'src/app/app.selectors'
import { selectUser } from 'src/app/pages/auth/auth.selectors'
import { AuthService } from 'src/app/pages/auth/services/auth.service'
import {
    deleteBilling,
    deleteBillingFail,
    deleteBillingSuccess,
    loadBillings,
    loadBillingsFail,
    loadBillingsSuccess,
    viewBilling,
} from 'src/app/pages/manage-payments/store/payments.actions'
import { saveBillingSuccess } from 'src/app/pages/new-payment/store/new-payment.actions'

@Injectable()
export class PaymentsEffects {
    billingsResolver$ = createEffect(() =>
        this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(this.store.pipe(select(selectUrl))),
            filter(([_, url]) => url && (url.includes('manage-payments') || url.includes('Checkout'))),
            map(() => loadBillings())
        )
    );

    saveBillingSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(saveBillingSuccess),
            map(() => loadBillings())
        )
    );

    viewBillingSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(viewBilling),
                tap(({ id }) =>
                    this.navController.navigateForward(`address/edit/${id}`)
                )
            ),
        { dispatch: false }
    );

    deleteBilling$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteBilling),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([{ id }, user]) =>
                this.authService.deleteBilling(user.id, id).pipe(
                    map(address => deleteBillingSuccess()),
                    catchError(error => of(deleteBillingFail({ error })))
                )
            )
        )
    );

    loadBilling$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadBillings),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([_, user]) =>
                this.authService.loadBillings(user.id).pipe(
                    map(billings => loadBillingsSuccess({ billings })),
                    catchError(error => of(loadBillingsFail({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private authService: AuthService,
        private store: Store<AppState>
    ) {}
}
