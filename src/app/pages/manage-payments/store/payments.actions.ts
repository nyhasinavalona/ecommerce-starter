import { HttpErrorResponse } from '@angular/common/http'
import { createAction, props } from '@ngrx/store'
import { Billing } from 'src/app/shared/interfaces/billing.interface'

export const loadBillings = createAction('LOAD_BILLINGS');
export const loadBillingsFail = createAction(
    'LOAD_BILLINGS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const loadBillingsSuccess = createAction(
    'LOAD_BILLINGS_SUCCESS',
    props<{ billings: Billing[] }>()
);
export const viewBilling = createAction(
    'VIEW_BILLING',
    props<{ id: number }>()
);
export const viewBillingFail = createAction(
    'VIEW_BILLING_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const viewBillingSuccess = createAction('VIEW_BILLING_SUCCESS');
export const deleteBilling = createAction(
    'DELETE_BILLING',
    props<{ id: number }>()
);
export const deleteBillingFail = createAction(
    'DELETE_BILLING_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const deleteBillingSuccess = createAction('DELETE_BILLING_SUCCESS');
