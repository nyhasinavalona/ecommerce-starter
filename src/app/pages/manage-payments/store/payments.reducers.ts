import { HttpErrorResponse } from '@angular/common/http'
import { createReducer, on } from '@ngrx/store'
import { loadBillingsFail, loadBillingsSuccess } from 'src/app/pages/manage-payments/store/payments.actions'
import { Billing } from 'src/app/shared/interfaces/billing.interface'

export interface PaymentsState {
    billings: Billing[];
    error: HttpErrorResponse;
}

export const initialState: PaymentsState = {
    billings: [],
    error: undefined
};

const reducer = createReducer(
    initialState,
    on(loadBillingsSuccess, (state, { billings }) => ({
        ...state,
        billings
    })),
    on(loadBillingsFail, (state, { error }) => ({ ...state, error }))
);

export function paymentsReducer(state, action) {
    return reducer(state, action);
}
