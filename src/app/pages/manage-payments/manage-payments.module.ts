import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { IonicModule } from '@ionic/angular'
import { ManagePaymentsPage } from 'src/app/pages/manage-payments/manage-payments.page'

const routes: Routes = [
    {
        path: '',
        component: ManagePaymentsPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ManagePaymentsPage]
})
export class ManagePaymentsPageModule {}
