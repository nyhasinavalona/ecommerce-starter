import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-email-settings',
    templateUrl: './email-settings.page.html',
    styleUrls: ['./email-settings.page.scss']
})
export class EmailSettingsPage implements OnInit {
    constructor(private menuCtrl: MenuController, private fun: UtilsService) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }
}
