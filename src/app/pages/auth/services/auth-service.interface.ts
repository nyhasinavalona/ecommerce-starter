import { Observable } from 'rxjs';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { User } from 'src/app/shared/models/user.model';

export interface AuthServiceInterface {
    signIn(user: User): Observable<User>;

    saveAddress(user: number, address: Address): Observable<Address>;
}
