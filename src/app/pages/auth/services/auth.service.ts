import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { AuthServiceInterface } from 'src/app/pages/auth/services/auth-service.interface'
import { Address } from 'src/app/shared/interfaces/address.interface'
import { ApiResponse } from 'src/app/shared/interfaces/api-response.interface'
import { Billing } from 'src/app/shared/interfaces/billing.interface'
import { User } from 'src/app/shared/models/user.model'
import { environment } from 'src/environments/environment'

@Injectable()
export class AuthService implements AuthServiceInterface {
    constructor(private http: HttpClient) {}

    signIn(user: User): Observable<User> {
        return this.http
            .post<ApiResponse>(`${environment.apiBaseUrl}/auth`, user)
            .pipe(
                map(response => response.data),
                map(data => new User().deserialize(data))
            );
    }

    saveAddress(user: number, address: Address): Observable<Address> {
        return this.http
            .post<ApiResponse>(`${environment.apiBaseUrl}/user/address`, {
                user,
                address
            })
            .pipe(map(response => response.data as Address));
    }

    deleteAddress(user: number, address: number): Observable<boolean> {
        return this.http
            .delete<ApiResponse>(
                `${environment.apiBaseUrl}/user/address/${address}`
            )
            .pipe(map(response => response.data as boolean));
    }

    loadAddressBook(user: number): Observable<Address[]> {
        return this.http
            .get<ApiResponse>(`${environment.apiBaseUrl}/user/address-book`)
            .pipe(map(response => response.data as Address[]));
    }

    saveBilling(user: number, billing: Billing): Observable<Billing> {
        return this.http
            .post<ApiResponse>(`${environment.apiBaseUrl}/user/billing`, {
                user,
                billing
            })
            .pipe(map(response => response.data as Billing));
    }

    deleteBilling(user: number, billing: number): Observable<boolean> {
        return this.http
            .delete<ApiResponse>(
                `${environment.apiBaseUrl}/user/billing/${billing}`
            )
            .pipe(map(response => response.data as boolean));
    }

    loadBillings(user: number): Observable<Billing[]> {
        return this.http
            .get<ApiResponse>(`${environment.apiBaseUrl}/user/billing`)
            .pipe(map(response => response.data as Billing[]));
    }
}
