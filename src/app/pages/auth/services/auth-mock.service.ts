import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { AuthServiceInterface } from 'src/app/pages/auth/services/auth-service.interface'
import { Address } from 'src/app/shared/interfaces/address.interface'
import { Billing } from 'src/app/shared/interfaces/billing.interface'
import { USER_MOCKS } from 'src/app/shared/mocks/user.mocks'
import { User } from 'src/app/shared/models/user.model'
import { BaseMockService } from 'src/app/shared/services/base-mock.service'

@Injectable()
export class AuthMockService extends BaseMockService<User>
    implements AuthServiceInterface {
    constructor() {
        super(User, USER_MOCKS);
    }

    signIn(payload: User): Observable<User> {
        const user = this.mocks.find(item => item.email === payload.email);
        if (!user) {
            throw new Error('User not found');
        }
        return of(user);
    }

    saveAddress(userId: number, address: Address): Observable<Address> {
        let user = this.mocks.find(item => item.id === userId);
        if (!user) {
            throw new Error('User not found');
        }
        let savedAddress = { ...address };
        if (!address.id) {
            savedAddress = { id: user.address.length + 1, ...address };
            user = new User().deserialize({
                ...user,
                address: [...user.address, savedAddress]
            });
        } else {
            user = new User().deserialize({
                ...user,
                address: [
                    ...user.address.filter(item =>
                        item.id === savedAddress.id ? savedAddress : item
                    )
                ]
            });
        }
        this.mocks = this.mocks.map(item =>
            item.id === user.id ? user : item
        );
        return of(savedAddress);
    }

    loadAddressBook(userId: number): Observable<Address[]> {
        const user = this.mocks.find(item => item.id === userId);
        if (!user) {
            throw new Error('User not found');
        }
        return of(user.address);
    }

    deleteAddress(user: number, address: number): Observable<boolean> {
        return of(true);
    }

    saveBilling(userId: number, payment: Billing): Observable<Billing> {
        let user = this.mocks.find(item => item.id === userId);
        if (!user) {
            throw new Error('User not found');
        }
        let savedBilling = { ...payment };
        if (!payment.id) {
            savedBilling = { id: user.billing.length + 1, ...payment };
            user = new User().deserialize({
                ...user,
                billing: [...user.billing, savedBilling]
            });
        } else {
            user = new User().deserialize({
                ...user,
                address: [
                    ...user.address.filter(item =>
                        item.id === savedBilling.id ? savedBilling : item
                    )
                ]
            });
        }
        this.mocks = this.mocks.map(item =>
            item.id === user.id ? user : item
        );
        return of(savedBilling);
    }

    loadBillings(userId: number): Observable<Billing[]> {
        const user = this.mocks.find(item => item.id === userId);
        if (!user) {
            throw new Error('User not found');
        }
        return of(user.billing);
    }

    deleteBilling(user: number, billing: number): Observable<boolean> {
        return of(true);
    }
}
