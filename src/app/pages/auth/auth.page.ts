import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { DataService } from 'src/app/data.service';
import { signIn } from 'src/app/pages/auth/auth.actions';
import { AuthState } from 'src/app/pages/auth/auth.reducers';
import { InfoModalPage } from 'src/app/pages/info-modal/info-modal.page';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-login',
    templateUrl: './auth.page.html',
    styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
    form: FormGroup;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private functionService: UtilsService,
        private menuCtrl: MenuController,
        private modalController: ModalController,
        private data: DataService,
        private formBuilder: FormBuilder,
        private store: Store<AuthState>
    ) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: [null, Validators.required],
            password: [null, Validators.required]
        });
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
        this.splashScreen.hide();
    }

    signIn() {
        this.platform.ready().then(() => {
            if (this.form.valid) {
                this.store.dispatch(signIn({ user: this.form.value }));
            } else {
                this.functionService.presentToast(
                    'Wrong Input!',
                    true,
                    'bottom',
                    2100
                );
            }
        });
    }

    async open_modal(b) {
        let modal;
        if (b) {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.data.terms_of_use,
                    title: 'Terms of Use'
                }
            });
        } else {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.data.privacy_policy,
                    title: 'Privacy Policy'
                }
            });
        }
        return await modal.present();
    }
}
