import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import {
    logOut,
    signInFail,
    signInSuccess
} from 'src/app/pages/auth/auth.actions';
import { User } from 'src/app/shared/models/user.model';

export interface AuthState {
    user: User;
    signInError: HttpErrorResponse;
}

export const initialState = {
    user: null,
    signInError: null
};

const reducer = createReducer(
    initialState,
    on(signInFail, (state, { error }) => ({ ...state, signInError: error })),
    on(signInSuccess, (state, { user }) => ({ ...state, user })),
    on(logOut, state => ({ ...state, user: undefined }))
);

export function authReducer(state, action) {
    return reducer(state, action);
}
