import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/shared/models/user.model';

export const signIn = createAction('SIGN_IN', props<{ user: User }>());
export const signInSuccess = createAction(
    'SIGN_IN_SUCCESS',
    props<{ user: User }>()
);
export const signInFail = createAction(
    'SIGN_IN_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const logOut = createAction('LOG_OUT');
