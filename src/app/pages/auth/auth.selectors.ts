import { createSelector } from '@ngrx/store';
import { selectAuthState } from 'src/app/app.selectors';

export const selectUser = createSelector(selectAuthState, state => state.user);
export const selectAddress = createSelector(selectUser, user => user.address);
