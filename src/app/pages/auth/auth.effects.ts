import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
    logOut,
    signIn,
    signInFail,
    signInSuccess
} from 'src/app/pages/auth/auth.actions';
import { AuthService } from 'src/app/pages/auth/services/auth.service';

@Injectable()
export class AuthEffects {
    signIn$ = createEffect(() =>
        this.actions$.pipe(
            ofType(signIn),
            switchMap(({ user }) =>
                this.authService.signIn(user).pipe(
                    map(payload => signInSuccess({ user: payload })),
                    catchError(error => of(signInFail({ error })))
                )
            )
        )
    );

    signInSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(signInSuccess),
                tap(() => {
                    this.navController.navigateForward(`/home`);
                })
            ),
        { dispatch: false }
    );

    logOut$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(logOut),
                tap(() => {
                    this.navController.navigateForward(`/auth`);
                    localStorage.clear();
                })
            ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private router: Router,
        private authService: AuthService
    ) {}
}
