import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-changepassword',
    templateUrl: './change-password.page.html',
    styleUrls: ['./change-password.page.scss']
})
export class ChangePasswordPage implements OnInit {
    constructor(public fun: UtilsService, private menuCtrl: MenuController) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }
}
