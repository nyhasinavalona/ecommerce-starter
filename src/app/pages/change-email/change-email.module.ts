import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChangeEmailPage } from 'src/app/pages/change-email/change-email.page';

const routes: Routes = [
    {
        path: '',
        component: ChangeEmailPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ChangeEmailPage]
})
export class ChangeEmailPageModule {}
