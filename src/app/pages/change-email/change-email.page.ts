import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { DataService } from 'src/app/data.service';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-change-email',
    templateUrl: './change-email.page.html',
    styleUrls: ['./change-email.page.scss']
})
export class ChangeEmailPage implements OnInit {
    constructor(
        public fun: UtilsService,
        private menuCtrl: MenuController,
        public dataService: DataService
    ) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }
}
