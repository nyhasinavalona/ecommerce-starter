import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { IonicModule } from '@ionic/angular'
import { AddressBookPage } from 'src/app/pages/address-book/address-book.page'
import { AuthMockService } from 'src/app/pages/auth/services/auth-mock.service'
import { AuthService } from 'src/app/pages/auth/services/auth.service'
import { environment } from 'src/environments/environment'

const routes: Routes = [
    {
        path: '',
        component: AddressBookPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [AddressBookPage],
    providers: [
        {
            provide: AuthService,
            useClass: environment.mock.auth ? AuthMockService : AuthService
        }
    ]
})
export class AddressBookPageModule {}
