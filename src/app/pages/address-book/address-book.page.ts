import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import {
    deleteAddress,
    loadAddressBook,
    viewAddress
} from 'src/app/pages/address-book/store/address-book.actions';
import { selectAddresses } from 'src/app/pages/address-book/store/address-book.selectors';
import { Address } from 'src/app/shared/interfaces/address.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-address-book',
    templateUrl: './address-book.page.html',
    styleUrls: ['./address-book.page.scss']
})
export class AddressBookPage implements OnInit {
    addresses$: Observable<Address[]>;

    constructor(
        private nav: NavController,
        public fun: UtilsService,
        public dataService: DataService,
        private menuCtrl: MenuController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {
        this.addresses$ = this.store.pipe(select(selectAddresses));
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(false, 'start');
    }

    addNewAddress() {
        this.nav.navigateForward('/address/new');
    }

    onEdit(id: number) {
        this.store.dispatch(viewAddress({ id }));
    }

    onDelete(id: number) {
        this.store.dispatch(deleteAddress({ id }));
    }
}
