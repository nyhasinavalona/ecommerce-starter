import { createSelector } from '@ngrx/store';
import { selectAddressBookState } from 'src/app/app.selectors';

export const selectAddresses = createSelector(
    selectAddressBookState,
    state => state.addresses
);
