import { Injectable } from '@angular/core'
import { NavController } from '@ionic/angular'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { routerNavigationAction } from '@ngrx/router-store'
import { select, Store } from '@ngrx/store'
import { of } from 'rxjs'
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators'
import { AppState } from 'src/app/app.reducers'
import { selectUrl } from 'src/app/app.selectors'
import {
    deleteAddress,
    deleteAddressFail,
    deleteAddressSuccess,
    loadAddressBook,
    loadAddressBookFail,
    loadAddressBookSuccess,
    viewAddress,
} from 'src/app/pages/address-book/store/address-book.actions'
import { selectUser } from 'src/app/pages/auth/auth.selectors'
import { AuthService } from 'src/app/pages/auth/services/auth.service'
import { saveAddress } from 'src/app/pages/new-address/store/new-address.actions'

@Injectable()
export class AddressBookEffects {
    addressBookResolver$ = createEffect(() =>
        this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(this.store.pipe(select(selectUrl))),
            filter(([_, url]) => url && (url.includes('address-book') || url.includes('Checkout'))),
            map(() => loadAddressBook())
        )
    );

    saveAddressSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(saveAddress),
            map(() => loadAddressBook())
        )
    );

    viewAddressSuccess$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(viewAddress),
                tap(({ id }) =>
                    this.navController.navigateForward(`address/edit/${id}`)
                )
            ),
        { dispatch: false }
    );

    deleteAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(deleteAddress),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([{ id }, user]) =>
                this.authService.deleteAddress(user.id, id).pipe(
                    map(address => deleteAddressSuccess()),
                    catchError(error => of(deleteAddressFail({ error })))
                )
            )
        )
    );

    loadAddressBook$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadAddressBook),
            withLatestFrom(this.store.pipe(select(selectUser))),
            switchMap(([_, user]) =>
                this.authService.loadAddressBook(user.id).pipe(
                    map(addresses => loadAddressBookSuccess({ addresses })),
                    catchError(error => of(loadAddressBookFail({ error })))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private navController: NavController,
        private authService: AuthService,
        private store: Store<AppState>
    ) {}
}
