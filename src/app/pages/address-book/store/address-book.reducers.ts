import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import {
    loadAddressBookFail,
    loadAddressBookSuccess
} from 'src/app/pages/address-book/store/address-book.actions';
import { Address } from 'src/app/shared/interfaces/address.interface';

export interface AddressBookState {
    addresses: Address[];
    error: HttpErrorResponse;
}

export const initialState: AddressBookState = {
    addresses: [],
    error: undefined
};

const reducer = createReducer(
    initialState,
    on(loadAddressBookSuccess, (state, { addresses }) => ({
        ...state,
        addresses
    })),
    on(loadAddressBookFail, (state, { error }) => ({ ...state, error }))
);

export function addressBookReducer(state, action) {
    return reducer(state, action);
}
