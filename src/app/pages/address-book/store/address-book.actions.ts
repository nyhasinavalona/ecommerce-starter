import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Address } from 'src/app/shared/interfaces/address.interface';

export const loadAddressBook = createAction(
    'LOAD_ADDRESS_BOOK'
);
export const loadAddressBookFail = createAction(
    'LOAD_ADDRESS_BOOK_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const loadAddressBookSuccess = createAction(
    'LOAD_ADDRESS_BOOK_SUCCESS',
    props<{ addresses: Address[] }>()
);
export const viewAddress = createAction(
    'VIEW_ADDRESS',
    props<{ id: number }>()
);
export const viewAddressFail = createAction(
    'VIEW_ADDRESS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const viewAddressSuccess = createAction(
    'VIEW_ADDRESS_SUCCESS'
);
export const deleteAddress = createAction(
    'DELETE_ADDRESS',
    props<{ id: number }>()
);
export const deleteAddressFail = createAction(
    'DELETE_ADDRESS_FAIL',
    props<{ error: HttpErrorResponse }>()
);
export const deleteAddressSuccess = createAction(
    'DELETE_ADDRESS_SUCCESS'
);
