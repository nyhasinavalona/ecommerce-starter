/**
 * Shoppr - E-commerce app starter Ionic 4(https://www.enappd.com)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source .
 *
 */
import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';
import { MenuController } from '@ionic/angular';

@Component({
    selector: 'app-passwordreset',
    templateUrl: './password-reset.page.html',
    styleUrls: ['./password-reset.page.scss']
})
export class PasswordResetPage implements OnInit {
    email = '';

    constructor(private fun: UtilsService, private menuCtrl: MenuController) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'start');
        this.menuCtrl.enable(false, 'end');
    }

    reset() {
        if (this.fun.validateEmail(this.email)) {
            this.fun.presentToast(
                'Verification mail sent',
                false,
                'bottom',
                2100
            );
        } else {
            this.fun.presentToast('Wrong Input!', true, 'bottom', 2100);
        }
    }
}
