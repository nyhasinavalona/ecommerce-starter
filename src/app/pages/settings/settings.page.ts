import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducers';
import { DataService } from 'src/app/data.service';
import { logOut } from 'src/app/pages/auth/auth.actions';
import { InfoModalPage } from 'src/app/pages/info-modal/info-modal.page';
import { SETTING_MENUS } from 'src/app/pages/settings/settings.constants';
import { Menu } from 'src/app/shared/interfaces/menu.interface';
import { UtilsService } from 'src/app/utils.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss']
})
export class SettingsPage implements OnInit {
    items: Menu[] = SETTING_MENUS;

    constructor(
        private navController: NavController,
        private fun: UtilsService,
        private menuCtrl: MenuController,
        private page: NavController,
        private dataService: DataService,
        private modalController: ModalController,
        private store: Store<AppState>
    ) {}

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(true, 'start');
    }

    nav(url: string) {
        if (url.includes('auth')) {
            this.store.dispatch(logOut());
        } else {
            this.navController.navigateForward(url);
        }
    }

    logout() {
        this.page.navigateRoot('/auth');
    }

    async openModal(b) {
        let modal;
        if (b) {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.dataService.terms_of_use,
                    title: 'Terms of Use'
                }
            });
        } else {
            modal = await this.modalController.create({
                component: InfoModalPage,
                componentProps: {
                    value: this.dataService.privacy_policy,
                    title: 'Privacy Policy'
                }
            });
        }
        return await modal.present();
    }
}
