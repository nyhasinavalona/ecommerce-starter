export const SETTING_MENUS = [
    { name: 'Notifications', url: '/notifications-settings' },
    // { name: 'Paramètres de messagerie', url: '/email-settings' },
    // { name: 'Paramètres de compte', url: '/account-settings' },
    { name: 'Gestion des adresses', url: '/address-book' },
    { name: 'Gestion des paiements', url: '/manage-payments' },
    { name: 'Déconnexion', url: '/auth' }
];
