/**
 * Shoppr - E-commerce app starter Ionic 4(https://www.enappd.com)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source .
 *
 */
import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { UtilsService } from 'src/app/utils.service';
import { DataService, Orders } from 'src/app/data.service';
import { OrderInformationPage } from 'src/app/pages/order-information/order-information.page';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.page.html',
    styleUrls: ['./orders.page.scss']
})
export class OrdersPage implements OnInit {
    orders: Array<Orders>;

    constructor(
        private menuCtrl: MenuController,
        private modalController: ModalController,
        private fun: UtilsService,
        private dataService: DataService
    ) {
        this.orders = dataService.orders;
    }

    ngOnInit() {}

    ionViewDidEnter() {
        this.menuCtrl.enable(false, 'end');
        this.menuCtrl.enable(true, 'start');
    }

    async open(order: Orders) {
        let modal = await this.modalController.create({
            component: OrderInformationPage,
            componentProps: { value: order }
        });
        return await modal.present();
    }
}
