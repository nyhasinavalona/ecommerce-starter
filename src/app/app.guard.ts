import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    RouterStateSnapshot
} from '@angular/router';
import { NavController } from '@ionic/angular';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from 'src/app/app.reducers';
import { selectUser } from 'src/app/pages/auth/auth.selectors';

@Injectable()
export class AppGuard implements CanActivate {
    constructor(
        private store: Store<AppState>,
        private navController: NavController
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.store.pipe(select(selectUser)).pipe(
            map(user => {
                const isLogged = !!user;
                if (!isLogged) {
                    this.navController.navigateForward(`/auth`);
                }
                return true;
            })
        );
    }
}
