import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppGuard } from 'src/app/app.guard';

const routes: Routes = [
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    {
        path: 'home',
        loadChildren: './pages/home/home.module#HomePageModule',
        canActivate: [AppGuard]
    },
    { path: 'auth', loadChildren: './pages/auth/auth.module#AuthPageModule' },
    {
        path: 'password-reset',
        loadChildren:
            './pages/password-reset/password-reset.module#PasswordResetPageModule'
    },
    {
        path: 'list',
        loadChildren: './pages/list/list.module#ListPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'info-modal',
        loadChildren: './pages/info-modal/info-modal.module#InfoModalPageModule'
    },
    {
        path: 'cart',
        loadChildren: './pages/cart/cart.module#CartPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'product-details',
        loadChildren:
            './pages/product-details/product-details.module#ProductDetailsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'search',
        loadChildren: './pages/search/search.module#SearchPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'notification',
        loadChildren:
            './pages/notification/notification.module#NotificationPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'address',
        loadChildren:
            './pages/new-address/new-address.module#NewAddressPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'Checkout',
        loadChildren: './pages/checkout/checkout.module#CheckoutPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'apply-promo',
        loadChildren:
            './pages/apply-promo/apply-promo.module#ApplyPromoPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'orders',
        loadChildren: './pages/orders/orders.module#OrdersPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'order-information',
        loadChildren:
            './pages/order-information/order-information.module#OrderInformationPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'faqs',
        loadChildren: './pages/faqs/faqs.module#FaqsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'faq',
        loadChildren: './pages/faq/faq.module#FaqPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'support',
        loadChildren: './pages/support/support.module#SupportPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'settings',
        loadChildren: './pages/settings/settings.module#SettingsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'notifications-settings',
        loadChildren:
            './pages/notifications-settings/notifications-settings.module#NotificationsSettingsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'email-settings',
        loadChildren:
            './pages/email-settings/email-settings.module#EmailSettingsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'address-book',
        loadChildren:
            './pages/address-book/address-book.module#AddressBookPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'manage-payments',
        loadChildren:
            './pages/manage-payments/manage-payments.module#ManagePaymentsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'payment',
        loadChildren:
            './pages/new-payment/new-payment.module#NewpaymentPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'account-settings',
        loadChildren:
            './pages/account-settings/account-settings.module#AccountSettingsPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'country',
        loadChildren: './pages/country/country.module#CountryPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'change-email',
        loadChildren:
            './pages/change-email/change-email.module#ChangeEmailPageModule',
        canActivate: [AppGuard]
    },
    {
        path: 'change-password',
        loadChildren:
            './pages/change-password/change-password.module#ChangePasswordPageModule',
        canActivate: [AppGuard]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
